public class MethodsExample {
    // defining a method that doesn't return anything (return type is void)
    // it just prints on the screen
    public static void printArray(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.println();
    }
    // defining a method that calculates a value and returns it
    public static int sumOfElements(int[] arr) {
        int sum = 0;
        for (int i = 0; i < arr.length; i++) {
            sum += arr[i];
        }
        return sum;
    }
    // defining a method that calculates a value and returns it
    public static double averageOfElements(int[] arr) {
        // calling a method from other method
        return (double) sumOfElements(arr) / arr.length;
    }

    public static void main(String[] args) {
        int[] myArray = {5, 6, 8, 11, 4, 5};
        // calling a method from other method
        printArray(myArray);
        // using the return value of a method
        System.out.println("Sum of elements is " + sumOfElements(myArray));
        System.out.println("Average of elements is " + averageOfElements(myArray));

    }
}
