package intl.sda.courses.java.advanced.lambda;

import intl.sda.courses.java.fundamentals.coding.arraysandmethods.ArrayUtils;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**Implement using lambda expressions:
 * The sum of elements (int type) of the list.
 */
public class LambdaExercises {
    public static void main(String[] args) {
        Function<List<Integer>, Integer> sumOfElements = list -> {
            Integer sum = 0;
            for (Integer element:
                 list) {
                sum += element;
            }
            return sum;
        };

        List<Integer> numbers = Arrays.asList(2,4,5,7,1,2,3,4);
        System.out.println("Sum of elements is "+sumOfElements.apply(numbers));

        //Number of words in the input expression (list containing elements of type
        //String).

        List<String> words = Arrays.asList("Tim", "Tam", "Pam", "Mam", "Sam", "Jam", "Wam");


    }

}
