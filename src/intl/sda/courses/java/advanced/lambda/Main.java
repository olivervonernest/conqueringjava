package intl.sda.courses.java.advanced.lambda;

import java.util.Random;
import java.util.function.*;

public class Main {
    public static void main(String[] args) {
        Person me = new Person("Oliver", 70);

        //without lambda expression
        Predicate<Person> adultPersonTest = new AdultPersonTest();
        System.out.println(me.getName() + " is" + (adultPersonTest.test(me) ? " an adult " : "a child"));

        // with lambda expressions
        Predicate<Person> adultPersonTestLambda = p -> p.getAge() >= 18;

        System.out.println(me.getName() + " is" + (adultPersonTestLambda.test(me) ? " an adult " : "a child"));

        // test if a person is of retiring age (age>=65)
        Predicate<Person> retiringAge = p -> p.getAge() >= 65;
        System.out.println(me.getName() + " is" + (retiringAge.test(me) ? "" : "not") + " of retiring age");

        // implementing Runnable interface using lambda expressions
        Runnable stopWatch = () -> {
            for (int i = 0; i < 10; i++) {
                System.out.println("Stop watch " + (i + 1));
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        // running as a separate thread
        //Thread stopWatchThrea = new Thread(stopWatch);
        //stopWatchThrea.start();

        // running in the current thread
        //stopWatch.run();

        // using Function interface with method apply to returns the length of a string
        // parameter type: String
        // return type: Integer

        Function<String, Integer> lengthOf = s -> s.length();
        System.out.println(lengthOf.apply("abdcde"));

        // using Function interface with method apply to replace all , with . from a string
        // parameter type: String
        // return type: String

        Function<String, String> replaceCommasWithDots = s -> s.replace(',', '.');
        System.out.println(replaceCommasWithDots.apply("a,b,c,d,e"));

        // using Function interface with method apply to returns the length of a string
        // using method reference
        // parameter type: String
        // return type: Integer

        Function<String, Integer> lengthOf2 = String::length;
        System.out.println(lengthOf2.apply("abdcde"));

        // Supplier interface with method get: has no parameters and returns a value
        Supplier<Integer> randomNumber = () -> new Random().nextInt();
        System.out.println("Generating a random number using Supplier interface: " + randomNumber.get());

        // Consumer interface with method accept: has a parameter and doesn't return anything
        Consumer<Double> printWIth2Decimals = (d) -> System.out.printf("%.2f", d);
        System.out.println("Printing a number with 2 decimals using Consumer interface: ");
        printWIth2Decimals.accept(Math.PI);
        System.out.println();

        //UnaryOperator interface with method apply: has a parameter
        // and parameter type and return type are the same
        UnaryOperator<Integer> toSquare = i -> i * i;
        System.out.println("6*6="+toSquare.apply(6));


    }
}
