package intl.sda.courses.java.advanced.lambda;

import java.util.function.BinaryOperator;

/**
 * a)Implement arithmetic operations using lambda expressions:
 * Addition, subtraction, multiplication, division.
 */
public class ArithmeticOperations {
    public static void main(String[] args) {
        double x = 5.9;
        double y = 4.8;
        // addition
        BinaryOperator<Double> add = (a, b) -> a + b;
        System.out.printf("%.2f + %.2f=%.2f%n", x, y, add.apply(x, y));

        //substraction
        BinaryOperator<Double> sub = (a, b) -> a - b;
        System.out.printf("%.2f - %.2f=%.2f%n", x, y, sub.apply(x, y));

        //multiplication
        BinaryOperator<Double> mult = (a, b) -> a * b;
        System.out.printf("%.2f * %.2f=%.2f%n", x, y, mult.apply(x, y));

        //divison
        BinaryOperator<Double> div = (a, b) -> a / b;
        System.out.printf("%.2f / %.2f=%.2f%n", x, y, div.apply(x, y));
    }
}
