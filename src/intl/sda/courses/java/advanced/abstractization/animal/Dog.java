package intl.sda.courses.java.advanced.abstractization.animal;

public class Dog extends Animal {

    public Dog(String name, String color, int age) {
        super(name, color, age);
    }

    @Override
    public String yieldVoice() {
        return "Woof";
    }
}
