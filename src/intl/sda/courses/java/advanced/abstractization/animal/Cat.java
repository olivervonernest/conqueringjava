package intl.sda.courses.java.advanced.abstractization.animal;

public class Cat extends Animal {
    public Cat(String name, String color, int age) {
        super(name, color, age);
    }

    @Override
    public String yieldVoice() {
        return "Meow";
    }
}
