package intl.sda.courses.java.advanced.abstractization.animal;

public class MainForAnimal {
    public static void main(String[] args) {
        Cat cat1 = new Cat("Kiisu", "black", 2);
        Dog dog1 = new Dog("Kuti", "black", 3);

        Animal[] animals = new Animal[]{cat1, dog1};
        for (int i = 0; i < animals.length; i++) {
            System.out.println(animals[i].yieldVoice());
            System.out.println(animals[i]);
        }

    }
}
