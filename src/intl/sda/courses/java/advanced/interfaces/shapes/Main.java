package intl.sda.courses.java.advanced.interfaces.shapes;

public class Main {
    public static void main(String[] args) {
        Shape[] shapes = {
                new Rectangle(4, 5),
                new Circle(3),
                new Square(6),
                new Triangle(3, 4, 5)};

        double totalPerimeter = 0;
        double totalArea = 0;

        for (Shape currentShape: shapes) {
            totalPerimeter += currentShape.getPerimeter();
            totalArea += currentShape.getArea();
            System.out.println(currentShape.printShape());
        }

        System.out.printf("%nTotal perimeter: %.2f%n", totalPerimeter);
        System.out.printf("%nTotal area: %.2f%n", totalArea);
    }
}
