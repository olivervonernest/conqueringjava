package intl.sda.courses.java.advanced.interfaces.shapes;

/*
All classes that implement this interface, must have getArea() and getPerimeter() methods.
 */
public interface Shape {
    double getArea();

    double getPerimeter();

    double[] getDimensions();

    default double getSemiPerimeter() {
        return getPerimeter() / 2;
    }

    default String printShape() {
        String s = this.getClass().getSimpleName().toLowerCase() + " (";
        double[] dims = this.getDimensions();
        for (int i = 0; i < dims.length; i++) {
            s += dims[i] + " ";

        }
        return s.trim() + ")";
    }
}
