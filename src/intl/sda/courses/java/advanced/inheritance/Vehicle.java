package intl.sda.courses.java.advanced.inheritance;

import java.util.Locale;

public class Vehicle {
    private int maxSpeed;

    public Vehicle(int maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public int getMaxSpeed() {
        return maxSpeed;
    }

    @Override
    public String toString() {
        // using getClass().getSimpleName() method from Object class (default parent class for calsse that dont extend a specific class)
        // to get the name
        return getClass().getSimpleName().toLowerCase() + " with maximum speed of " + maxSpeed +
                " km/h";
    }
}
