package intl.sda.courses.java.advanced.io;

import java.io.*;
import java.nio.file.Paths;

public class FilesExample {
    public static void main(String[] args) throws IOException {
        // get current directory before Java 8
        String cwd = System.getProperty("user.dir");
        System.out.println("Current working directory: " + cwd);

        // get current directory after Java 8
        String path = Paths.get("").toAbsolutePath().toString();
        System.out.println("Current working directory: " + path);

        File myFirstFile = new File("res/file1.txt");
        System.out.println(myFirstFile.getAbsolutePath());

        // if BufferedWriter is created like this:
        // BufferedWriter writer = new BufferedWriter(new FileWriter(myFirstFile,true)
        //the new content will be appended to the existing file.
        // if BufferedWriter is created like this:
        // BufferedWriter writer = new BufferedWriter(new FileWriter(myFirstFile)
        //the new content will be overwrite the content of the existing file.
        try(BufferedWriter writer = new BufferedWriter(new FileWriter(myFirstFile,true))) {
            String line = "Some text\n";
            writer.write(line);
        }

        try (BufferedReader reader = new BufferedReader(new FileReader(myFirstFile))) {
            String line;
            while ((line = reader.readLine()) != null) {
                System.out.println(line);
            }
        }
    }
}
