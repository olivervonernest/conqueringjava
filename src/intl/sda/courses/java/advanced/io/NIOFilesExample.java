package intl.sda.courses.java.advanced.io;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.List;

public class NIOFilesExample {
    // nio stands for non-blocking input-output operations
    public static void main(String[] args) throws IOException {
        Path file = Paths.get("res/file2.txt");

        List<String> lines = Arrays.asList("First line", "Second line", "Third line");
        // if the file already exists, it overwrites the first characters from the file with ones provided by lines variable
        Files.write(file, lines, StandardOpenOption.CREATE);

        lines = Files.readAllLines(file);
        System.out.println(lines);
    }
}
