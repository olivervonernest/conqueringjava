package intl.sda.courses.java.advanced.io.homework0603;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Read a list of colors from a text file (each color on a row, colors can be repeated).
 * Display only distinct colors.
 * Hint: use nio classes to read from the file (file should be already created) and
 * HashSet to store the colors.
 */
public class Colors {
    public static void main(String[] args) throws IOException {
        Path file = Paths.get("res/colors.txt");
        List<String> lines;
        /*
        lines = Arrays.asList("Yellow","Blue", "White", "Red", "Orange", "Green", "Yellow");
        Files.write(file, lines, StandardOpenOption.CREATE);

         */

        lines = Files.readAllLines(file);

        Set<String> colorsFromFile = new HashSet<>();
        colorsFromFile.addAll(lines);

        System.out.println("Colors from file:");
        for (String colors: colorsFromFile) {
            System.out.println(colors);
        }

    }
}
