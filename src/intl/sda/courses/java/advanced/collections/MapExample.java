package intl.sda.courses.java.advanced.collections;

import java.util.HashMap;
import java.util.Map;

public class MapExample {
    public static void main(String[] args) {
        // Map -> HashMap is a class that implements the Map interface
        // a map stores unique pairs of (key, value); order is not preserved
        // create a map that stores countries as keys and population in millions as values


        Map<String, Double> countries = new HashMap<>();
        countries.put("Romania", 18.0);
        countries.put("Estonia", 1.5);
        countries.put("Nigeria", 200.0);
        countries.put("China", 1400.0);
        //countries.put("Romania", 23.0); kui panna uuesti, siis võtab viimasena pandud value


        //to iterate over a map use entrySet() method to get the collection of entries
        //Map.Entry is the type of the entry (pari of key and value)
        //Entry is an inner type in type Map (Map is an interface)
        for (Map.Entry<String, Double> entry:
             countries.entrySet()) {
            System.out.println(entry.getKey()+" has a "+entry.getValue()+" million people");
        }
    }
}
