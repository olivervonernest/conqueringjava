package intl.sda.courses.java.advanced.collections.homework0603;

import java.util.HashMap;
import java.util.Map;

/**
 * Create a shopping list using a HashMap.
 * It should contain items to buy as keys and quantities as values.
 * Add elements to the shopping list, remove elements from it and print the shopping list.
 */
public class ShoppingList {
    public static void main(String[] args) {
        Map<String, Integer> shoppingList = new HashMap<>();
        shoppingList.put("Milk", 1);
        shoppingList.put("Bread", 2);
        shoppingList.put("Apples", 5);
        shoppingList.put("Cheese", 1);
        shoppingList.put("Lemonade", 2);
        shoppingList.remove("Lemonade");

        System.out.println("You must buy:");
        for (Map.Entry<String, Integer> entry :
                shoppingList.entrySet()) {
            System.out.println(entry.getValue() + " " + entry.getKey());
        }


    }
}
