package intl.sda.courses.java.advanced.collections;

import java.util.*;

public class ListAndSetExample {
    public static void main(String[] args) {

        // List -> Arraylist is a class that implements List interface
        // in a list adding order is preserved and it can contain duplicates
        List<String> travelRoute = new ArrayList<>();
        travelRoute.add("Romania");
        travelRoute.add("Transilvania");
        travelRoute.add("Estonia");
        travelRoute.add("Hungary");
        travelRoute.add("Bulgaria");
        travelRoute.add("Latvia");
        travelRoute.add("Sweden");
        travelRoute.add("Estonia");
        travelRoute.add("Romania");

        travelRoute.remove("Romania");

        Iterator<String> iterator = travelRoute.iterator();

        System.out.println("Travel route: ");
        while (iterator.hasNext()) {
            System.out.print(iterator.next());
            if (iterator.hasNext()) {
                System.out.print("->");
            }else {
                System.out.println();
            }
        }

        // Set -> HashSet is a class that implements the Set interface
        // in a set order is not preserved and it doesn't contain duplicates

        Set<String> visitedCountries = new HashSet<>();
        visitedCountries.add("Romania");
        visitedCountries.add("Estonia");
        visitedCountries.add("Latvia");
        visitedCountries.add("Lithuania");
        visitedCountries.add("Canada");
        visitedCountries.add("Romania");
        //visitedCountries.remove("Romania");

        System.out.println("Visited countries:");
        for (String country:
             visitedCountries) {
            System.out.println(country);
        }
    }
}
