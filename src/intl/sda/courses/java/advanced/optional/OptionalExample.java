package intl.sda.courses.java.advanced.optional;

import java.util.Optional;

public class OptionalExample {
    public static void main(String[] args) {
        String text1 = "Some text";
        String text2 = null;

        // using an optional for non-null value
        Optional<String> stringOptional1 = Optional.of(text1);
        System.out.println("Optional with a non-null value:");
        System.out.println(stringOptional1);
        System.out.println(stringOptional1.isPresent());
        System.out.println(stringOptional1.get());

        System.out.println();
        // using an optional for a null value
        Optional<String> stringOptional2 = Optional.ofNullable(text2);
        System.out.println("Optional with null value");
        System.out.println(stringOptional2);
        System.out.println(stringOptional2.isPresent());
        // when the value might be null use method orElse() instead of get() to get the value
        System.out.println(stringOptional2.orElse("no value here"));
    }
}
