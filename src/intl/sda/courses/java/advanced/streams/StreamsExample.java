package intl.sda.courses.java.advanced.streams;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.stream.Collectors;

public class StreamsExample {
    public static void main(String[] args) {
        List<String> names = Arrays.asList("Oliver", "Maiu", "Kadi", "Renee", "Keira", "Mihkel", "Martin", "Aron", "Helen", "Sander");

        System.out.println("List of names: ");
        System.out.println(names);

        System.out.println("List of names starting with M:");
        List<String> namesStartingWithM = names.stream()
                .filter(name -> name.startsWith("M")).collect(Collectors.toList());
        System.out.println(namesStartingWithM);

        System.out.println("The lengths of each name from the list:");
        List<Integer> namesLength = names.stream().map(String::length).collect(Collectors.toList());
        System.out.println(namesLength);

        System.out.println("The average length of names:");
        OptionalDouble averageNameLength = namesLength.stream().mapToInt(Integer::intValue).average();
        averageNameLength.ifPresent(d -> System.out.printf("%.2f%n", d));

        System.out.println("Do all names have at least 5 letters?");
        System.out.println(names.stream().allMatch(s -> s.length() >= 5));

        System.out.println("Is there any name with more than 7 letters?");
        System.out.println(names.stream().anyMatch(s->s.length() > 7));

        System.out.println("Concatenated names:");
        Optional<String> namesConcatenation = names.stream().reduce((currValue, element)-> currValue+=", "+element);
        System.out.println(namesConcatenation.orElse("empty list of names"));

        System.out.println("List of names with lengths:");
        names.stream().forEach(s-> System.out.println(s+" has "+s.length()+" letters"));

        List<Person> persons = Arrays.asList(
                new Person("Oliver", "Ernest", 30),
                new Person("Jana", "Hanama", 45),
                new Person("Kando", "Junilimo", 34),
                new Person("Kame", "Hameha", 22),
                new Person("Tiit","Mangustiit", 78)
        );
        System.out.println("List of persons:");
        System.out.println(persons);

        System.out.println("\nList of persons sorted by last name:");
        persons.stream().sorted((p1, p2)->p1.getLastName().compareTo(p2.getLastName())).forEach(System.out::println);

        System.out.println("\nList of persons sorted by age ascending:");
        persons.stream().sorted((p1, p2)->p1.getAge()-p2.getAge()).forEach(System.out::println);

        System.out.println("\nList of persons sorted by age descending:");
        persons.stream().sorted((p1, p2)->p2.getAge()-p1.getAge()).forEach(System.out::println);
        // or with minus sign
        //persons.stream().sorted((p1, p2)->-(p1.getAge()-p2.getAge())).forEach(System.out::println);




    }
}
