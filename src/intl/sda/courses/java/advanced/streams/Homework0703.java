package intl.sda.courses.java.advanced.streams;


import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Using streams , for a given lists
 * --[„John”, „Sarah”, „Mark”, „Tyla”, Ellisha ”, Eamonn"]
 * --[1, 4, 2346, 123, 76, 11, 0, 0, 62, 23,50]
 * a)
 * Sort the list.
 * b)
 * Print only those names , that start with „E” letter
 * c)
 * Print values greater than 30 and lower than 200.
 * d)
 * Print names uppercase
 * e)
 * Remove first and last letter , sort and print names
 * f)
 * f)*Sort backwards by implementing reverse Comparator and using lambda
 * expression
 */
public class Homework0703 {
    public static void main(String[] args) {
        List<String> names = Arrays.asList("John", "Sarah", "Mark", "Tyla", "Ellisha", "Eamonn");
        List<Integer> numbers = Arrays.asList(1, 3, 2346, 123, 76, 0, 0, 62, 23, 50);

        // a)
        System.out.println("Sorting the names and numbers:");
        List<String> sortedNames = names.stream().sorted(String::compareTo).collect(Collectors.toList());
        System.out.println(sortedNames);
        List<Integer> sortedNumbers = numbers.stream().sorted((a, b) -> a.compareTo(b)).collect(Collectors.toList());
        System.out.println(sortedNumbers);

        System.out.println("\nPrinting the names that start with E:");
        // b)
        List<String> namesWithE = names.stream().filter(name -> name.startsWith("E")).collect(Collectors.toList());
        System.out.println(namesWithE);

        System.out.println("\nPrinting the numbers >30 and <200");
        // c)
        List<Integer> someValues = numbers.stream().filter(num -> num > 30 && num < 200).sorted().collect(Collectors.toList());
        System.out.println(someValues);


        System.out.println("\nPrinting the names uppercase");
        // d)
        List<String> namesToUpperCase = names.stream().map(String::toUpperCase).collect(Collectors.toList());
        System.out.println(namesToUpperCase);

        System.out.println("\nRemoving first and the last letter");
        // e)
        List<String> nameWithoutFirstLast = names.stream().filter(s -> s.length() > 2).map(s -> s.substring(1, s.length() - 1)).collect(Collectors.toList());
        System.out.println(nameWithoutFirstLast);

        List<String> reversedList = names.stream().sorted(Comparator.reverseOrder()).collect(Collectors.toList());
        System.out.println();
        System.out.println(reversedList);

    }
}
