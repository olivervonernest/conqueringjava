package intl.sda.courses.java.advanced.serialization;

import java.io.*;
import java.time.LocalDate;

public class SerializationExample {
    public static void main(String[] args) throws IOException {
        File personFile = new File("res/persons.dat");

        ObjectOutputStream output = new ObjectOutputStream(new FileOutputStream(personFile));
        output.writeObject(new Person("Anna", "Karenina", LocalDate.of(1990, 12, 24)));
        output.writeObject(new Person("Juno", "Banananas", LocalDate.of(1955, 6, 12)));
        output.writeObject(new Person("Mambo", "Jambo", LocalDate.of(1987, 2, 13)));
        output.close();

        ObjectInputStream input = new ObjectInputStream(new FileInputStream(personFile));
        Person p;
        boolean eof = false;

        System.out.println("File content:");
        while (!eof) {
            try {
                p = (Person) input.readObject();
                System.out.println(p);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }catch (EOFException e) {
                eof = true;
            }
        }
        input.close();

    }
}
