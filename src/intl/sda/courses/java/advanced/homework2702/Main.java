package intl.sda.courses.java.advanced.homework2702;

/**
 * Ex 1. Create classes Dog and Cat with fields name, age, color.
 * a) Move common methods and fields to the class Animal
 * b) Create method yieldVoice (this method should print "woof" for dog and "meow" for cat)
 * c) Create method toString that types the name of the class and the value of the fields in class Animal.
 * It should work for Dog and Cat classes too.
 * d) Create simple array of type Animal , that will contain one object of type Dog and one object of type Cat
 * e) Using for each loop show which animal gives what kind of voice.
 */
public class Main {
    public static void main(String[] args) {
        Cat cat1 = new Cat("Kiisu", "black", 2);
        Dog dog1 = new Dog("Kuti", "black", 3);

        Animal[] animals = new Animal[]{cat1, dog1};
        for (int i = 0; i < animals.length; i++) {
            animals[i].yieldVoice();
        }

        System.out.println(cat1);
        System.out.println(dog1);


    }
}
