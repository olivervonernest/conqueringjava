package intl.sda.courses.java.advanced.homework2702;

public class Cat extends Animal {
    public Cat(String name, String color, int age) {
        super(name, color, age);
    }

    @Override
    public void yieldVoice() {
        System.out.println("Meow");
    }
}
