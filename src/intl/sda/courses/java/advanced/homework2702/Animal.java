package intl.sda.courses.java.advanced.homework2702;

public class Animal {
    private String name;
    private String color;
    private int age;

    public Animal(String name, String color, int age) {
        this.name = name;
        this.color = color;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void yieldVoice() {
        System.out.println("Some noise");
    }
    //    Ex For a Cat object with field values name: Tom, color: black, age: 2
//    toString should return Tom is a black cat 2 years old
    @Override
    public String toString() {
        return getName() + " is a " + getColor().toLowerCase() + " " + getClass().getSimpleName().toLowerCase() + " who is " + getAge() + " years old.";
    }

}
