package intl.sda.courses.java.advanced.enums;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

       int unitOrdinal;
       LengthUnit unit;
       double value;
       int newUnitOrdinal;
       LengthUnit newUnit;


        System.out.println("CHoose unit to convert from:");
        UnitConvertor.showMenu();
        unitOrdinal = scanner.nextInt();
        unit = LengthUnit.values()[unitOrdinal];

        System.out.println("Input value in "+unit+": ");
        value = scanner.nextDouble();

        System.out.println("CHoose unit to convert from:");
        UnitConvertor.showMenu();
        newUnitOrdinal = scanner.nextInt();
        newUnit = LengthUnit.values()[newUnitOrdinal];

        System.out.println(value + " " + unit + " = "+UnitConvertor.convert(value, unit, newUnit) + " "+newUnit);


    }
}
