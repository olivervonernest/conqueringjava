package intl.sda.courses.java.advanced.enums;

public class UnitConvertor {
    public static void showMenu(){
        for (LengthUnit lengthUnit : LengthUnit.values()) {
            System.out.println(lengthUnit.ordinal()+" -> "+lengthUnit.name());
        }
    }

    public static double convert(double value, LengthUnit unit, LengthUnit newUnit) {
        double valueInMeters = value * unit.getValueInMeters();
        double valueInNewUnit = valueInMeters / newUnit.getValueInMeters();
        return valueInNewUnit;
    }
}
