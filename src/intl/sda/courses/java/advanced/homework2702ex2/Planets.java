package intl.sda.courses.java.advanced.homework2702ex2;

/**
 * Create enum Planets. A planet should have a name,
 * a relative size (small, medium, large, huge) and distanceFromEarth fields.
 * a) Override toString method . It should print relative size of a planet and it’s name .
 * E.g . Huge Jupiter”, „Small Pluto”.
 * b) Create distanceFromEarth method
 * c) Verify both methods for multiple planets
 */
public enum Planets {
    // similar to declaring a variable of type Planet and initializing it:
    // Planet pluto = new Planet("small", 7500);
    // but PLUTO is also a constant -> values don't change after initialization
    PLUTO("small", 7500),
    JUPITER("huge", 700),
    EARTH("medium", 0),
    MARS("medium", 219);
    private String size;
    private double distanceFromEarth; // million of kilometers
    Planets(String size, double distanceFromEarth) {
        this.size = size;
        this.distanceFromEarth = distanceFromEarth;
    }
    public double getDistanceFromEarth() {
        return distanceFromEarth;
    }
    @Override
    public String toString() {
        return capitalize(size)+" "+capitalize(this.name());
    }
    private String capitalize(String stringToCapitalize) {
//        String result;
//        // extracting the first letter of the string and capitalizing it
//        // (transforming it to upper case)
//        result = stringToCapitalize.substring(0,1).toUpperCase();
//        // extracting the rest of the string (all letters except of the first letter)
//        // and transforming them to lower case
//        result += stringToCapitalize.substring(1).toLowerCase();
//        return result;
        return stringToCapitalize.substring(0,1).toUpperCase()
                + stringToCapitalize.substring(1).toLowerCase();
    }
}
