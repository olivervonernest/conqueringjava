package intl.sda.courses.java.advanced.homework2702ex2;

public class MainForPlanets {
    public static void main(String[] args) {
        System.out.println(Planets.JUPITER +
                " "+ Planets.JUPITER.getDistanceFromEarth()+" millions of km from Earth");
        System.out.println(Planets.MARS +
                " "+ Planets.MARS.getDistanceFromEarth()+" millions of km from Earth");
    }
}
