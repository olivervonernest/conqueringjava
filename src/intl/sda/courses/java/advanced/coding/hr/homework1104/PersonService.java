package intl.sda.courses.java.advanced.coding.hr.homework1104;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

public class PersonService {

    public static void runApplication(String fileRelativePath, String separator) {
        try {
            List<Persoonid> persons = loadPersonsFromFile(fileRelativePath, separator);
            int option = 0;
            Scanner scanner = new Scanner(System.in);
            do {
                showMenu();
                option = scanner.nextInt();
                switch (option) {
                    case 1:
                        listPersons(persons);
                        break;
                    case 2:
                        addPerson(persons);
                        break;
                    case 3:
                        updatePerson(persons);
                        break;
                    case 4:
                        removePerson(persons);
                        break;
                    case 5:
                        break;
                    case 6:
                        break;
                    default:
                        System.out.println("Invalid option");
                }


            } while (option != 5 && option != 6);
            if (option == 5) {
                try {
                    savePersonsToFile(persons, fileRelativePath, separator);
                    System.out.println("Data saved succesfully.");
                } catch (IOException e) {
                    System.out.println("Unable to read the specified file: " + fileRelativePath);
                }
            }
        } catch (IOException e) {
            System.out.println("Unable to read the specified file: " + fileRelativePath);
        }
    }

    public static List<Persoonid> removePerson(List<Persoonid> persons) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Choose the person to remove by entering the line number: ");
        int lineNo = readLineNo(persons.size());

        if (lineNo > 0) {
            Persoonid personToRemove = persons.get(lineNo - 1);
            System.out.println("Person to update: ");
            System.out.println(lineNo + ": " + personToRemove);
            persons.remove(lineNo - 1);
            System.out.println("Person removed succesfully");
        }
        return persons;
    }

    private static void showMenu() {
        System.out.println("Choose an option:");
        System.out.println("1 - List persons");
        System.out.println("2 - Add persons");
        System.out.println("3 - Update");
        System.out.println("4 - Remove");
        System.out.println("5 - Exit & Save");
        System.out.println("6 - Exit without saving");
    }

    private static void listPersons(List<Persoonid> persons) {
        for (int i = 0; i < persons.size(); i++) {
            System.out.println((i + 1) + ": " + persons.get(i));
        }
    }

    private static int readLineNo(int noOfLines) {
        Scanner scanner = new Scanner(System.in);
        int lineNo = scanner.nextInt();
        if (lineNo > 0 && lineNo <= noOfLines) {
            return lineNo;
        }
        System.out.println("Invalid line");

        return 0;
    }

    private static List<Persoonid> addPerson(List<Persoonid> persons) {

        persons.add(readPersonData(false));
        System.out.println("Person added successfully");

        return persons;
    }

    private static String capitalize(String s) {
        return s.substring(0, 1).toUpperCase(Locale.ROOT) + s.substring(1);
    }
    /*
    Using reflection to copy fields that are not null and not empty from personWithUpdatedFields to personToUpdate
     */
    private static Persoonid updateFields(Persoonid personToUpdate, Persoonid personWithUpdatedFields) {

        //getting the fields of the person of the Persoonid class
        Field[] fields = personToUpdate.getClass().getDeclaredFields();
        for (int i = 0; i < fields.length; i++) {

            Field field = fields[i];

            try {
                // we are copying just the non-static fields /instance bound fields
                if (!Modifier.isStatic(field.getModifiers())) {
                    //getting the getter method for the current field
                    Method getter = Persoonid.class.getMethod("get" + capitalize(field.getName()));
                    // getting the value of the private field using the getter
                    Object value = getter.invoke(personWithUpdatedFields);
                    if (value != null) {
                        //getting the setter method for the current field
                        Method setter = Persoonid.class.getMethod("set" + capitalize(field.getName()), value.getClass());
                        if (value instanceof String) {
                            String s = (String) value;
                            if (s.length() > 0) {
                                // setting the value of a field the setter
                                setter.invoke(personToUpdate, value);
                            }

                        } else {
                            setter.invoke(personToUpdate, value);
                        }
                    }

                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        }


        return personToUpdate;
    }

    private static List<Persoonid> updatePerson(List<Persoonid> persons) {
        Scanner scanner = new Scanner(System.in);
        String firstName, lastName, dateOfBirthString, country, city, address;
        LocalDate dateOfBirth = null;


        System.out.print("Choose the person to update by entering the line number: ");
        int lineNo = readLineNo(persons.size());


        if (lineNo > 0) {
            Persoonid personToUpdate = persons.get(lineNo - 1);
            System.out.println("Person to update: ");
            System.out.println(lineNo + ": " + personToUpdate);
            System.out.println("Input new info for the person. Press enter if the field value remains unchanged");
            Persoonid personWithOnlyUpdatedFields = readPersonData(true);
            updateFields(personToUpdate, personWithOnlyUpdatedFields);
            System.out.println("Person update successfully");

        }

        return persons;
    }

    private static Persoonid readPersonData(boolean forUpdate) {
        Scanner scanner = new Scanner(System.in);
        String firstName, lastName, dateOfBirthString, country, city, address;
        LocalDate dateOfBirth = null;

        System.out.println("First name: ");
        firstName = scanner.nextLine();
        System.out.println("Last name: ");
        lastName = scanner.nextLine();
        do {
            System.out.println("Date of birth (yyyy-mm-dd): ");
            dateOfBirthString = scanner.nextLine();
            if (forUpdate && dateOfBirthString.length() == 0) {
                // if readPersonData method us used in update context (forUpdate == true)
                // the an empty String for a date is acceptable because it means
                // dateOFBirth remains unchanged so we can get out of while loop
                break;
            }
            try {
                dateOfBirth = LocalDate.parse(dateOfBirthString);
            } catch (DateTimeParseException e) {
                System.out.println("Invalid date.");
            }
        } while (dateOfBirth == null);
        System.out.println("Country: ");
        country = scanner.nextLine();
        System.out.println("City: ");
        city = scanner.nextLine();
        System.out.println("Address");
        address = scanner.nextLine();

        return new Persoonid(firstName, lastName, dateOfBirth, country, city, address);
    }

    public static List<Persoonid> loadPersonsFromFile(String fileRelativePath, String separator) throws IOException {
        List<String> lines = Files.readAllLines(Paths.get(fileRelativePath));
        List<Persoonid> persons = new ArrayList<>();
        for (String line :
                lines) {
            persons.add(Persoonid.parse(line, separator));
        }
        return persons;
    }

    public static void savePersonsToFile(List<Persoonid> persons, String fileRelativePath, String separator) throws IOException {
        List<String> lines = new ArrayList<>();
        Path filePath = Paths.get(fileRelativePath);
        for (Persoonid p :
                persons) {
            lines.add(p.toString(separator));
        }
        if (Files.exists(filePath)) {
            Files.delete(filePath);
        }
        Files.write(filePath, lines, StandardOpenOption.CREATE_NEW);
    }


}
