package intl.sda.courses.java.advanced.coding.hr;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class MainForPerson {
    public static void main(String[] args) throws IOException {

        // reading the persons data from the txt file
        // each line contains the data for a person, each field is separated by comma and space
        List<String> lines = Files.readAllLines(Paths.get("res/persons.txt"));

        //using a set to identify the unique persons
        HashSet<Person> uniquePersons = new HashSet<>();
        for (String line : lines) {
            String[] fields = line.split(", ");
            String firstName = fields[0];
            String lastName = fields[1];
            //String dateOfBirth = fields[2];
            //LocalDate newDateOfBirth = LocalDate.parse(dateOfBirth);
            LocalDate dateOfBirth = LocalDate.parse(fields[2]);
            String country = fields[3];
            String city = fields[4];
            String address = fields[5];


            Person p = new Person(firstName, lastName, dateOfBirth, country, city, address);
            uniquePersons.add(p);

        }
        // saving the unique persons in a new file
        // data of each Person need sto be int he form of a string,
        // with each field separated by comma and space like in the initial file
        List<String> uniqueLines = new ArrayList<>();
        for (Person p: uniquePersons
             ) {
            uniqueLines.add(p.toString());
        }
        Files.write(Paths.get("res/unique-persons.txt"), uniqueLines, StandardOpenOption.CREATE );


    }
}
