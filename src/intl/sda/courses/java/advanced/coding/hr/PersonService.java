package intl.sda.courses.java.advanced.coding.hr;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class PersonService {
    public static List<Person> loadPersonsFromFile(String fileRelativePath, String separator) throws IOException {
        List<String> lines = Files.readAllLines(Paths.get(fileRelativePath));
        List<Person> persons = new ArrayList<>();
        for (String line :
                lines) {
            persons.add(Person.parse(line, separator));
        }
        return persons;
    }
    public static void savePersonsToFile(List<Person> persons, String fileRelativePath, String separator) throws IOException {
        List<String> lines = new ArrayList<>();
        Path filePath = Paths.get(fileRelativePath);
        for (Person p :
                persons) {
            lines.add(p.toString(separator));
        }
        if (Files.exists(filePath)) {
            Files.delete(filePath);
        }
        Files.write(filePath, lines, StandardOpenOption.CREATE_NEW);
    }
    /**
     * Based on a list of persons that may contain duplicate values,
     * it generates a LinkedHashMap with a person as a key and a list of persons as a value.
     * The list of persons contains the values encountered for the same person.
     * Ex. persons are considered the same person if they have the same firstName, lastName
     * and birthOfDate, but country, city and address may vary.
     *
     * @param persons
     * @return
     */
    public static LinkedHashMap<Person, List<Person>> identifyOccurences(List<Person> persons) {
        LinkedHashMap<Person, List<Person>> occurencesMap = new LinkedHashMap<>();
        for (Person p :
                persons) {
            List<Person> duplicates = new ArrayList<>();
            if (occurencesMap.containsKey(p)) {
                duplicates = occurencesMap.get(p);
            }
            duplicates.add(p);
            occurencesMap.put(p, duplicates);
        }
        return occurencesMap;
    }

}
