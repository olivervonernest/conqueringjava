package intl.sda.courses.java.advanced.coding.hr;

import java.io.IOException;
import java.util.*;

/*
    Create an application that will process a text file containing
    data about persons (firstName, lastName, dateOfBirth, country, city, address):
    - it will load the data into Person objects
    - it will order the entries by firstName, lastName and birthOfDate
    - it will identify the duplicate entries:
        Persons are considered the same person if they have the same firstName, lastName
        and birthOfDate, but country, city and address may vary
    - it will ask the user to chose which entry to keep
    - it will produce to output files with ordered entries:
        persons-unique.txt containing unique entries
        and persons-discarded.txt containing the discarded duplicate entries
 */

public class MainMain {
    public static void main(String[] args) throws IOException {
        List<Person> personsFromFile =
                PersonService.loadPersonsFromFile("res/persons.txt", Person.DEFAULT_SEPARATOR);
        personsFromFile.sort(Comparator.naturalOrder());
        LinkedHashMap<Person, List<Person>> occurencesMap = PersonService.identifyOccurences(personsFromFile);
        System.out.println("DUPLICATE ENTRIES\n");
        List<Person> uniqueEntries = new ArrayList<>();
        List<Person> discardedEntries = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);
        // for each entry (person) with more than one value the user is asked which entry to keep
        // in the end the entries/lines/persons from the initial file will be split into
        // 2 files - unique and discarded
        for (Map.Entry<Person, List<Person>> entry :
                occurencesMap.entrySet()) {
            List<Person> duplicateEntries = entry.getValue();
            if (duplicateEntries.size() > 1) {
                System.out.println("Which entry do you want to keep?");
                for (int i = 0; i < duplicateEntries.size(); i++) {
                    System.out.println((i + 1) + " -> " + duplicateEntries.get(i));
                }
                // assuming the user inputs a correct option
                int option = scanner.nextInt();
                for (int i = 0; i < duplicateEntries.size(); i++) {
                    if (i+1 == option) {
                        uniqueEntries.add(duplicateEntries.get(i));
                    } else {
                        discardedEntries.add(duplicateEntries.get(i));
                    }
                }
            } else {
                uniqueEntries.add(entry.getKey());
            }
        }
        PersonService.savePersonsToFile(uniqueEntries, "res/persons-unique.txt", Person.DEFAULT_SEPARATOR);
        PersonService.savePersonsToFile(discardedEntries, "res/persons-discarded.txt", Person.DEFAULT_SEPARATOR);
        System.out.println("File processed successfully.");



    }
}
