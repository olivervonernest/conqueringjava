package intl.sda.courses.java.advanced.coding.hr.homework1104;

import intl.sda.courses.java.advanced.coding.hr.Person;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Stream;

public class Service {

    public static void showPersonsOnScreen(String fileRelativePath, String separator) throws IOException {
        List<Persoonid> showPersonList = Service.loadPersonsFromFile(fileRelativePath, separator);

        System.out.println("LIST OF PERSONS:");
        showPersonList.sort(Comparator.naturalOrder());
        for (Persoonid showList : showPersonList) {
            System.out.println(showList);
        }
    }

    public static void showMenu() {
        System.out.println("Choose an option:");
        System.out.println("1 - List persons");
        System.out.println("2 - Add persons");
        System.out.println("3 - Update");
        System.out.println("4 - Remove");
        System.out.println("5 - Exit");
    }

    public static List<Persoonid> loadPersonsFromFile(String fileRelativePath, String separator) throws IOException {
        List<String> lines = Files.readAllLines(Paths.get(fileRelativePath));
        List<Persoonid> persons = new ArrayList<>();
        for (String line :
                lines) {
            persons.add(Persoonid.parse(line, separator));}
        return persons;
    }

    public static void savePersonsToFile(List<Persoonid> persons, String fileRelativePath, String separator) throws IOException {
        List<String> lines = new ArrayList<>();
        Path filePath = Paths.get(fileRelativePath);
        for (Persoonid p :
                persons) {
            lines.add(p.toString(separator));
        }
        if (Files.exists(filePath)) {
            Files.delete(filePath);
        }
        Files.write(filePath, lines, StandardOpenOption.CREATE_NEW);
    }

    public static Persoonid addNewPerson() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("First name");
        String firstName = scanner.next();
        System.out.println("Last name");
        String lastName = scanner.next();
        System.out.println("Year");
        int year = scanner.nextInt();
        System.out.println("Month");
        int month = scanner.nextInt();
        System.out.println("Day");
        int day = scanner.nextInt();
        System.out.println("Country");
        String country = scanner.next();
        System.out.println("City");
        String city = scanner.next();
        System.out.println("Street number");
        String number = scanner.next();
        System.out.println("Street name");
        String streetName = scanner.next();

        String address = number +" "+ streetName;

        return new Persoonid(firstName, lastName, LocalDate.of(year, month, day),country, city, address);

    }
}
