package intl.sda.courses.java.advanced.coding.hr.homework1104;

import intl.sda.courses.java.advanced.coding.hr.Person;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.LocalDate;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) throws IOException {
        PersonService.runApplication("res/allthepersons.txt", Persoonid.DEFAULT_SEPARATOR);

    }
}
