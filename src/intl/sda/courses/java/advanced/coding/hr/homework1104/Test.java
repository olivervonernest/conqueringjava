package intl.sda.courses.java.advanced.coding.hr.homework1104;

import java.io.IOException;
import java.util.List;

public class Test {
    public static void main(String[] args) throws IOException {
        List<Persoonid> personsFromFile = Service.loadPersonsFromFile("res/allthepersons.txt", ",");
        System.out.println(personsFromFile);
    }
}
