package intl.sda.courses.java.advanced.encapsulation;

import intl.sda.courses.java.advanced.encapsulation.oop.Pocket;

public class MainForPocket {
    public static void main(String[] args) {
        // creating a pocket object and initialize field money with value 10
        Pocket pocket = new intl.sda.courses.java.advanced.encapsulation.oop.Pocket(10);
        // for values smaller or equal than 10, getMoney() returns 0
        System.out.println("Money in my pocket: "+pocket.getMoney());
        // setMoney() will accept only values between 0 and 3000, for other values it only prints a message
        pocket.setMoney(-23);
        System.out.println("Money in my pocket: "+pocket.getMoney()+ " value is unchanged");
        pocket.setMoney(4444);
        System.out.println("Money in my pocket: "+pocket.getMoney()+" value is unchanged");
        // when the value is correct setMoney() chancges the value of the field money inside the pocket
        pocket.setMoney(345);
        System.out.println("Money in my pocket: "+pocket.getMoney());

    }
}
