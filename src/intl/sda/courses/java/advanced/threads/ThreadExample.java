package intl.sda.courses.java.advanced.threads;

public class ThreadExample {
    public static void main(String[] args) throws InterruptedException {
        StopWatchThread stopWatch1 = new StopWatchThread("sw1");
        StopWatchThread stopWatch2 = new StopWatchThread("sw2");
        // to start a new thread of execution we need to call start method
        stopWatch1.start();
        stopWatch2.start();

        System.out.println("Main thread starts");
        Thread.sleep(5000);
        System.out.println("Main thread is still running");
        Thread.sleep(5000);
        System.out.println("Main thread ends");



    }
}
