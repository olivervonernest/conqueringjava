package intl.sda.courses.java.advanced.threads;

public class StopWatchThread extends Thread {

    public StopWatchThread(String name) {
        super(name);
    }

    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            System.out.println("Stop watch " + this.getName() + ": " +(i+1));
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
