package intl.sda.courses.java.advanced.threads;

public class MainForSeatTaker {
    public static void main(String[] args) {
        Bench bench = new Bench(5);
        SeatTakerThread seatTaker1 = new SeatTakerThread(bench, "Anna");
        SeatTakerThread seatTaker2 = new SeatTakerThread(bench, "John");
        SeatTakerThread seatTaker3 = new SeatTakerThread(bench, "Mary");
        SeatTakerThread seatTakes4 = new SeatTakerThread(bench, "Oliver");
        seatTaker1.start();
        seatTaker2.start();
        seatTaker3.start();
        seatTakes4.start();
    }
}
