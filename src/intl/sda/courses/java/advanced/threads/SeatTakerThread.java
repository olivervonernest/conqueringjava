package intl.sda.courses.java.advanced.threads;

public class SeatTakerThread  extends Thread {
    private Bench bench;

    public SeatTakerThread(Bench bench, String name) {
        super(name);
        this.bench = bench;
    }

    @Override
    public void run() {
         bench.takeASeat(getName());

    }
}
