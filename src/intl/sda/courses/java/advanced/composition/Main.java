package intl.sda.courses.java.advanced.composition;

/**
 * Write an application that consists of few classes:
 * a) Author class, representing an author – poem writer,
 * which consists of fields surname and nationality (both of type String) b)
 * Poem class, representing poem, which consists of fields creator (type Author) and
 * stropheNumbers (type int – numbers of strophes in poem) c)
 * Main class, with main method, inside which you will:
 * i. Create three instances of Poem class,
 * fill them with data (using constructor and/or setters) and
 * store them in array
 * ii. Write a surname of an author,
 * that wrote a longest poem (let your application calculate it!)
 */
public class Main {
    public static void main(String[] args) {
        // using an object of type Author enables us to add more than one poem with the same author
        Author eminescu = new Author("Eminescu", "romanian");
        Poem[] poems = new Poem[4];
        poems[0] = new Poem(new Author("Shakespeare", "british"), 10);
        poems[1] = new Poem(eminescu, 98);
        poems[2] = new Poem(new Author("Vasta", "nigerian"), 20);
        // adding another poem belonging to the same author (Eminescu)
        poems[3] = new Poem(eminescu, 8);
        System.out.println("The author with the poem with the gratest number of strophes is: ");
        System.out.println(Poem.getAuthorNameOfLongestPoem(poems));
    }
}
