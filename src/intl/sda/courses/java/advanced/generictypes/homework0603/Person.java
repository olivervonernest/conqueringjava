package intl.sda.courses.java.advanced.generictypes.homework0603;


/**
 * Create a Person class that will implement a Comparable interface . Person class should
 * implement compareTo method , that will verify if one person is taller than another
 */
public class Person implements Comparable<Person>{
    private int height;
    private String name;

    public Person(int height, String name) {
        this.height = height;
        this.name = name;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int compareTo(Person anotherPerson) {
        return this.height - anotherPerson.height;
    }


}
