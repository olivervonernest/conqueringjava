package intl.sda.courses.java.advanced.generictypes.homework0603;

import java.util.Arrays;

public class MainForSomeGenericClass {
    public static void main(String[] args) {


        SomeGenericClass<String> someString = new SomeGenericClass<>("Jaanus");
        SomeGenericClass<Integer> someInteger = new SomeGenericClass<>(5);
        SomeGenericClass<Double> someDouble = new SomeGenericClass<>(344.22);

        SomeGenericClass[] someArray = {someString, someInteger, someDouble};
        for (int i = 0; i < someArray.length; i++) {
            System.out.println(someArray[i].getSomeValue());
        }


    }
}
