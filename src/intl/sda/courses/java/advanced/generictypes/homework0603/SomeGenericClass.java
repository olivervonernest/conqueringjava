package intl.sda.courses.java.advanced.generictypes.homework0603;

/**
 *
 * Create a simple Generic class , that will give a possibility , to store any kind of value
 * within . Add object of type String, Integer and Double to array of that Generic type . Print
 * all values of the array within a loop
 */
public class SomeGenericClass<V> {
    private V someValue;

    public SomeGenericClass(V someValue) {
        this.someValue = someValue;
    }

    public V getSomeValue() {
        return someValue;
    }

    public void setSomeValue(V someValue) {
        this.someValue = someValue;
    }
}
