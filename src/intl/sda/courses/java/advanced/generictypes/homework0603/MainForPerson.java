package intl.sda.courses.java.advanced.generictypes.homework0603;

import java.nio.charset.StandardCharsets;
import java.util.Locale;

public class MainForPerson {
    public static void main(String[] args) {
        Person person1 = new Person(189, "Taavi");
        Person person2 = new Person(186, "Jaanus");

        System.out.println(person1.getName()+" is "
                +(person1.compareTo(person2) == 0 ? "same height as " :
                (person1.compareTo(person2) > 0 ? "higher than " : "smaller than "))
                + person2.getName());
    }
}
