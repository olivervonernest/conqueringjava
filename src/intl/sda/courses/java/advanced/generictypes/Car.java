package intl.sda.courses.java.advanced.generictypes;

public class Car implements Comparable<Car>{
    private String model;
    private int maxSpeed;

    public Car(String model, int maxSpeed) {
        this.model = model;
        this.maxSpeed = maxSpeed;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(int maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    /**
     *
     * @param anotherCar
     * @return 0 if the objects are equal,
     * or a positive value if current objects is bigger than object compared to
     * or a negative value if current object is smaller than the object compared to
     * A car is greater than another car if max speed is greater.
     */
    @Override
    public int compareTo(Car anotherCar) {
        return this.maxSpeed - anotherCar.maxSpeed;
    }
}
