package intl.sda.courses.java.advanced.generictypes;

public class MainForGenericTypes {
    public static void main(String[] args) {

        Car someCar = new Car("Ford Fiesta", 160);
        Dog someDog = new Dog("Toto", 10);

        // box with an item (of any class/type) implemented with generic types
        GenericBox<Car> carGenericBoxWithACar = new GenericBox<>(someCar);
        System.out.println("This box contains a someCar of model " + carGenericBoxWithACar.getItem().getModel());

        GenericBox<Dog> dogGenericBoxWithADog = new GenericBox<>(someDog);
        System.out.println("This box contains a someDog with the name "+ dogGenericBoxWithADog.getItem().getName());

        // box with an item (of any class/type) implemented without generic types
        Box boxWithACar = new Box(someCar);
        System.out.println("This box contains a someCar of model " + ((Car) boxWithACar.getItem()).getModel());

        Box boxWithADog = new Box(someDog);
        System.out.println("This box contains a someDog with the name " + ((Dog) boxWithADog.getItem()).getName());


        // comparing two cars using interface Comparable<T>

        Car car1 = new Car("Ford Fiesta", 190);
        Car car2 = new Car("Audi Allroad", 170);

        System.out.println(car1.getModel()+" is "
                +(car1.compareTo(car2) == 0 ? "as fast as " :
                (car1.compareTo(car2) > 0 ? "faster than " : "slower than "))
                + car2.getModel());

    }


}
