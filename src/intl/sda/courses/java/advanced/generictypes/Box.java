package intl.sda.courses.java.advanced.generictypes;

public class Box {
    private Object item;

    public Box(Object item) {
        this.item = item;
    }

    public Object getItem() {
        return item;
    }

    public void setItem(Object item) {
        this.item = item;
    }
}
