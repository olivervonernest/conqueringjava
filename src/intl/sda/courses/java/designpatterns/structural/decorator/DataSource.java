package intl.sda.courses.java.designpatterns.structural.decorator;

public interface DataSource {
    void writeData(String data);
    String readData();
}
