package intl.sda.courses.java.designpatterns.structural.compositevol2;

public interface Department {
    void printDepartmentName();
}
