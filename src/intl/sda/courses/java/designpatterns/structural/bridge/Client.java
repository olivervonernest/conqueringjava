package intl.sda.courses.java.designpatterns.structural.bridge;

public class Client {
    public static void main(String[] args) {
        Device tv = new TV();
        BasicRemote remote = new BasicRemote(tv);
        remote.power();
        tv.printStatus();
        remote.power();
        tv.printStatus();

        Device radio = new Radio();
        remote = new BasicRemote(radio);
        remote.power();
        radio.printStatus();
        remote.power();
        radio.printStatus();

    }
}
