package intl.sda.courses.java.designpatterns.structural.proxy;

import java.util.HashMap;

public interface ThirdPartyVideoLib {
    HashMap<String, Video> popularVideos();

    Video getVideo(String videoId);
}
