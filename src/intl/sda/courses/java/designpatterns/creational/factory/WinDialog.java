package intl.sda.courses.java.designpatterns.creational.factory;

public class WinDialog extends Dialog{
    @Override
    public Button createButton() {
        return new WindowsButton();
    }
}
