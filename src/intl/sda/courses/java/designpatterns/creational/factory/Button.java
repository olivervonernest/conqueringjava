package intl.sda.courses.java.designpatterns.creational.factory;

public interface Button {
    void paint();

}
