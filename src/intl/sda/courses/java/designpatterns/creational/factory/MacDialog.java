package intl.sda.courses.java.designpatterns.creational.factory;

public class MacDialog extends Dialog{
    @Override
    public Button createButton() {
        return new MAcButton();
    }
}
