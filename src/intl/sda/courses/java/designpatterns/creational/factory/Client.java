package intl.sda.courses.java.designpatterns.creational.factory;

public class Client {
    private static Dialog dialog;

    public static void main(String[] args) {
        if (System.getProperty("os.name").equals("Windows 10")) {
            dialog = new WinDialog();
        } else if (System.getProperty("os.name").equals("Mac")) {
            dialog = new MacDialog();
        } else {
            dialog = new HTMLDialog();
        }
        dialog.renderWindow();
    }
}
