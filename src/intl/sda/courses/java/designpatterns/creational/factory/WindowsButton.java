package intl.sda.courses.java.designpatterns.creational.factory;

public class WindowsButton implements Button{
    @Override
    public void paint() {
        System.out.println("This is a Windows button");
    }
}
