package intl.sda.courses.java.designpatterns.creational.factory;

public class MAcButton implements Button{
    @Override
    public void paint() {
        System.out.println("This is a Mac button");
    }
}
