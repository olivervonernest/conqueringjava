package intl.sda.courses.java.designpatterns.creational.factory;

public class HTMLButton implements Button{


    @Override
    public void paint() {
        System.out.println("This is HTML button");
    }
}
