package intl.sda.courses.java.designpatterns.creational.builder;


public class CarBuilder implements Builder {
    String carType;
    int seats;
    String carColor;

    @Override
    public void setCarType(String carType) {
        this.carType = carType;

    }

    @Override
    public void setSeats(int seats) {
        this.seats = seats;

    }

    @Override
    public void setCarColor(String carColor) {
        this.carColor = carColor;

    }
}
