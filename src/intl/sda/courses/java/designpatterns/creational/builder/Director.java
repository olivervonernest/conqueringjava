package intl.sda.courses.java.designpatterns.creational.builder;

public class Director {
    public void createSUVCar(Builder builder) {
        builder.setCarColor("Blue");
        builder.setCarType("SUV");
        builder.setSeats(5);
    }
}
