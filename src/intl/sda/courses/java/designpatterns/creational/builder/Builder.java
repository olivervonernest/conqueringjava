package intl.sda.courses.java.designpatterns.creational.builder;

public interface Builder {
    void setCarType(String carType);
    void setSeats(int seats);
    void setCarColor(String carColor);

}
