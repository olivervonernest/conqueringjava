package intl.sda.courses.java.designpatterns.creational.builder;

public class Car extends CarBuilder {
    String carType;
    int seats;
    String carColor;

    public Car(String carType, int seats, String carColor) {
        this.carType = carType;
        this.seats = seats;
        this.carColor = carColor;
    }
}
