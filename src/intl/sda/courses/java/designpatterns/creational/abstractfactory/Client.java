package intl.sda.courses.java.designpatterns.creational.abstractfactory;

public class Client {
    public static Application configure() {
        Application application;
        GUIFactory factory;
        String oSName = System.getProperty("os.name").toLowerCase();
        if(oSName.contains("windows 10")){
            factory = new WindowsFactory();
        }else {
            factory = new HTMLFactory();
        }
        application = new Application(factory);
        return application;
    }

    public static void main(String[] args) {
        Application application = configure();
        application.paintButton();
        application.paintCheckBox();
    }
}
