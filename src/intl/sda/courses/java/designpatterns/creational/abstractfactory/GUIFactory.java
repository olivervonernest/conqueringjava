package intl.sda.courses.java.designpatterns.creational.abstractfactory;

public interface GUIFactory {
    Button createButton();
    CheckBox createCheckBox();
}
