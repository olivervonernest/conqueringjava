package intl.sda.courses.java.designpatterns.creational.abstractfactory;

public class HTMLFactory implements GUIFactory{
    @Override
    public Button createButton() {
        return new HTMLButton();
    }

    @Override
    public CheckBox createCheckBox() {
        return (CheckBox) new HTMLCheckBox();
    }
}
