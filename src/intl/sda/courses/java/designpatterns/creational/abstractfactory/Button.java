package intl.sda.courses.java.designpatterns.creational.abstractfactory;

public interface Button {
    void paint();
}
