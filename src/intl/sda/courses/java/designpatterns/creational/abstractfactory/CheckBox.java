package intl.sda.courses.java.designpatterns.creational.abstractfactory;

public interface CheckBox {
    void paint();
}
