package intl.sda.courses.java.designpatterns.creational.abstractfactory;

public class HTMLButton implements Button{
    @Override
    public void paint() {
        System.out.println("This is a HTML button");
    }
}
