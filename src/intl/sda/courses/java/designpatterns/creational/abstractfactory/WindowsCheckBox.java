package intl.sda.courses.java.designpatterns.creational.abstractfactory;

public class WindowsCheckBox implements Button{
    @Override
    public void paint() {
        System.out.println("This is a Windows checkbox");
    }
}
