package intl.sda.courses.java.designpatterns.creational.abstractfactory;

public class HTMLCheckBox implements Button{
    @Override
    public void paint() {
        System.out.println("This is a HTML Checkbox");
    }
}
