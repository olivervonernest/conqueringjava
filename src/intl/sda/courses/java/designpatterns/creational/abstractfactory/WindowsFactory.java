package intl.sda.courses.java.designpatterns.creational.abstractfactory;

public class WindowsFactory implements GUIFactory {
    @Override
    public Button createButton() {
        return new WindowsButton();
    }

    @Override
    public CheckBox createCheckBox() {
        return (CheckBox) new WindowsCheckBox();
    }
}
