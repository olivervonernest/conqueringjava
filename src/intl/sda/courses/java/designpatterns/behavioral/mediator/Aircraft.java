package intl.sda.courses.java.designpatterns.behavioral.mediator;

public interface Aircraft {
    void land();
}
