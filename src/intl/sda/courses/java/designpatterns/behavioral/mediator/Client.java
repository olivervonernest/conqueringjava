package intl.sda.courses.java.designpatterns.behavioral.mediator;

public class Client {
    public static void main(String[] args) {
        ControlTower controlTower = new ControlTower();
        Boeing boeing456 = new Boeing(controlTower);
        boeing456.requestControlTower();
        controlTower.run();
    }
}
