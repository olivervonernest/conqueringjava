package intl.sda.courses.java.fundamentals.coding.staticexamples;

/**
 * Let's say we have a class Dog.
 * The class itself (a blueprint of a dog in the real world) is NOT an object
 * but only a blueprint for the objects it will represent
 * (specific things/beings like my dog, your dog, foods like pizza, coffee, etc..).
 * If you have a dog named Fluffy, it is not a blueprint (class)
 * - Fluffy is an OBJECT (instance) of the blueprint (class) Dog
 * because it can have very different characteristics compared to say,
 * your dog Sparky.
 *
 * So difference between STATIC vs NON-STATIC?
 *
 * STATIC:
 * Every dog makes a barking sound - it doesn't belong to a specific dog -
 * it belongs to the blueprint (class) of a Dog.
 *
 * ON-STATIC:
 * Our dog has a specific name (among other things like color, size, personality): Fluffy.
 * Since that name belongs to OUR Fluffy and not to the blueprint (class) Dog,
 * it must be a non-static field, because every dog has a distinct name
 * (even if we have 2 dogs with the same name, the rest of the dogs will still have different names!)
 * Non-static fields belong to every single OBJECT (instance) - and Fluffy is our ehrm.. object!
 */
public class Dog {
    // Static field (also a constant - final keyword - the value doesn't change after initialization.
    // Static fields are linked to the class and have the same value for all the objects.
    public static final String SPECIES = "Canis lupus familiaris";

    //Non-static fields (object specific fields)
    private String name;
    private int age;
    private String color;

    public Dog(String name, int age, String color) {
        this.name = name;
        this.age = age;
        this.color = color;
    }

    //Non-static methos (object spercific methods)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    // Static method - is linked to the class and works the same for all objects

    public static String bark() {
        return "woof woof!";
    }
}
