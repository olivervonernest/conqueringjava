package intl.sda.courses.java.fundamentals.coding.staticexamples;

public class MainForDog {
    public static void main(String[] args) {
        Dog fluffy = new Dog("Fluffy", 3, "white");
        Dog maia = new Dog("Maia", 5, "Brown");

        System.out.println(fluffy.getName() + ": " + Dog.bark());
        System.out.println(maia.getName() + ": " + Dog.bark());

        System.out.println(fluffy.getName() + " and "+ maia.getName()+" are dogs (species: " + Dog.SPECIES + ")");

    }
}
