package intl.sda.courses.java.fundamentals.coding.staticexamples;

import java.util.Random;

public class Horse {

    public static int getNoOfHorses() {
        return noOfHorses;
    }

    private static int noOfHorses = 0;
    private static Horse winnerOfRace;

    private String name;
    private int speed;

    public Horse(String name, int speed) {
        this.name = name;
        this.speed = speed;
        noOfHorses++;
        if (winnerOfRace == null) {
            winnerOfRace = this;
        } else {
            if (this.speed > winnerOfRace.speed) {
                winnerOfRace = this;
            }
        }
        System.out.println(this);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public static Horse getWinnerOfRace() {
        return winnerOfRace;
    }

    @Override
    public String toString() {
        return name + " " + speed + " km/h";
    }

    public static void setUpRace() {
        String[] names = {"Black beauty", "Spots", "Lucky", "Mambo", "Jambo", "Dumbo", "John", "Oliver", "kdsokd", "fijsdpoif"};

        Random random = new Random();
        int noOfHorses = random.nextInt(9) + 2; // random generates numbers from 0 to 8; by adding 2 we have numbers 2 to 10
        for (int i = 1; i <= noOfHorses; i++) {
            int nameIndex = random.nextInt(10);
            int nameRandomNumber = 100 + random.nextInt(900); //numbers from 100 to 99
            String name = names[nameIndex] + " " + nameRandomNumber;
            int speed = 50 + random.nextInt(21);
            Horse horse = new Horse(name, speed);
        }
    }
}
