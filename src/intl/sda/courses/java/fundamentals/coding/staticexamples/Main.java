package intl.sda.courses.java.fundamentals.coding.staticexamples;

public class Main {
    public static void main(String[] args) {

        Horse.setUpRace();

        System.out.println("Number of the horses in the race: " + Horse.getNoOfHorses());

        System.out.println("winner of the race is: "+ Horse.getWinnerOfRace());

    }
}
