package intl.sda.courses.java.fundamentals.coding.homework0702;

public class Circle {
    /**
     * Create class Circle with field radius of type double.
     * Create methods calculatePerimeter and calculateArea and test them in a separate class.
     * Note: use Math.PI for the value of pi.
     */
    double radius;


    public double calculatePerimeter() {
        return 2 * (Math.PI * radius);
    }

    public double calculateArea() {
        return Math.PI * (radius * radius);
    }

    public Circle(double radius) {
        this.radius = radius;
    }


}
