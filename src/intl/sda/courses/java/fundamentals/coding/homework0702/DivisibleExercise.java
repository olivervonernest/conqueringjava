package intl.sda.courses.java.fundamentals.coding.homework0702;

public class DivisibleExercise {
    /**
     * Write a Java program that takes a number and prints its multiplication table up to 100: 8
     * Output: 8, 16, 24, 32, …, 80, 88, 96
     */
    public static void multiplicationTable(int num) {
        int i = 1;
        while (num * i <= 100) {
            System.out.print(num * i + " ");
            i++;
        }
        System.out.println();
    }
    /*
    Write a Java program to print numbers between 1 to 100 which are divisible by a, b and both (a*b).
     */
    public static void divisibleAB(int a, int b) {
        // numbers divisible by a up to 100 are actually the multiplication table of a to 100
        multiplicationTable(a);
        multiplicationTable(b);
        // numbers divisible by a and b up to 100 are actually the multiplication table of a*b to 100
        multiplicationTable(a*b);
    }
    public static void main(String[] args) {
        divisibleAB(3, 5);
        divisibleAB(2, 5);
    }

}
