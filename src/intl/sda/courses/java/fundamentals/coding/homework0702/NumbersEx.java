package intl.sda.courses.java.fundamentals.coding.homework0702;

import org.w3c.dom.ls.LSOutput;

import java.util.Scanner;

public class NumbersEx {


    public void multiplicationTable(int num) {
        /**
         * Write a Java program that takes a number and prints its multiplication table up to 100: 8
         * Output: 8, 16, 24, 32, …, 80
         */

        int i = 1;
        while (num * i <= 100); {
            System.out.print(num * i + ", ");
            i++;
        }

    }

    public void numberPrinter(int n) {
        /**
         * Write a Java program that accepts an integer (n) and displays n, nn, nnn, nnnn, nnnnnn: 5
         * Output: 5, 55, 555, 5555, 55555
         */

        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= i; j++) {
                System.out.print(n);
            }
            if (i != n) {
                System.out.print(", ");
            }
        }
    }
    public void ex3(int num1, int num2) {
        /**
         * Write a Java program to print numbers between 1 to 100 which are divisible by 3, 5 and both
         * Divided by 3: 3, 6, 9, 12, 15, 18, 21, 24, 27, 30, 33, 36, 39, 42, 45, 48, 51, 54, 57, 60, 63, 66, 69,
         * 72, 75, 78, 81, 84, 87, 90, 93, 96, 99
         * Divided by 5: 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 80, 85, 90, 95
         * Divided by 3 & 5: 15, 30, 45, 60, 75, 90
         */

        System.out.println("\nDivided by "+num1);
        for (int i=1; i<=100; i++) {
            if (i%num1==0)
                System.out.print(i +" ");
        }

        System.out.println("\n\nDivided by "+num2);
        for (int i=1; i<=100; i++) {
            if (i%num2==0) System.out.print(i +" ");
        }

        System.out.println("\n\nDivided by "+num1+" and "+num2);
        for (int i=1; i<=100; i++) {
            if (i%num1==0 && i%num2==0) System.out.print(i +" ");
        }
        System.out.println("\n");
    }

    public void ex4(int num) {
        /**
         * Write a Java program that prints all the powers of a number under 100. Ex: for 3
         * Output: 3, 9, 27, 81
         */
        /*
        int exponent = 7; //you have to choose exponent, that makes number power over 100
        int power = 1;


        for (int i = 1; i <= exponent; i++) {
            power = power * num;
            if (power <= 100) {
                //then i limit it's power with 100
                System.out.print(power+" ");
            }

        }
         */

        /*
        System.out.print("Powers of number " + num + ": ");
        int i = num;
        if (num <= 10) {
            for (i = num; i < 100; i *= num) {
                System.out.print(i + ", ");
            }
        } else {
            System.out.println("Number is too big!");
        }

         */



        double result = num;
        int n = 1;
        while (result < 100) {
            System.out.print(result+" ");
            n++;
            result = Math.pow(num, n);
        }
    }



}
