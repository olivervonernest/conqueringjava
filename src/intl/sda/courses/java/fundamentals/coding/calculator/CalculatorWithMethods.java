package intl.sda.courses.java.fundamentals.coding.calculator;

import java.util.Scanner;

// a) Declare and initialize three variables of type int: a, b, c (initialize it to any values)
// b)Store the result of the a - b - c operation in the variable result1 and then display it on the standard output (screen)
// c)Declare and initialize three variables of type long: d, e, f (initialize it to any values)
// d)Store the result of the d * e / f operation in the variable result2 and
// then display it on the standard output (screen)

/* Calculator application
 * Read int a and int b from the keyboard.
 * Display a menu that allows user to choose an operation to perform:
 * 1) Calculate a+b.
 * 2) Calculate a-b
 * 3) Calculate a*b
 * 4) Calculate a/b
 * 5) Exit.
 * Let the user input the operation (from 1 to 5),
 * display the result and then the menu until the user chooses 5.
 */

public class CalculatorWithMethods {

    public static int showMenuAndReadOperation() {
        int operation;
        System.out.println("\n Available operations: ");
        System.out.println(" 1) Calculate A + B");
        System.out.println(" 2) Calculate A - B");
        System.out.println(" 3) Calculate A * B");
        System.out.println(" 4) Calculate A / B");
        System.out.println(" 5) Exit");
        System.out.println("\n Choose an operation to perform: ");
        Scanner scan = new Scanner(System.in);
        operation = scan.nextInt();
        return operation;
    }

    public static void performOperation(int operation) {
        Scanner scan = new Scanner(System.in);
        double a;
        double b;
        if (operation < 1 || operation > 5) {
            System.out.println("Unsupported action!");
            return;
        }
        System.out.print("Input A: ");
        a = scan.nextDouble();
        System.out.print("Input B: ");
        b = scan.nextDouble();
        switch (operation) {
            case 1:
                System.out.println("A + B = " + (a + b));
                break;
            case 2:
                System.out.println("A - B = " + (a - b));
                break;
            case 3:
                System.out.println("A * B = " + (a * b));
                break;
            case 4:
                System.out.println("A / B = " + (a / b));
                break;
        }
    }

    }
