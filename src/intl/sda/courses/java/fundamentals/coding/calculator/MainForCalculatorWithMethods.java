package intl.sda.courses.java.fundamentals.coding.calculator;

public class MainForCalculatorWithMethods {
    public static void main(String[] args) {
        int operation;
        do {
            operation = CalculatorWithMethods.showMenuAndReadOperation();
            if (operation != 5) {
                CalculatorWithMethods.performOperation(operation);
            }
        } while (operation != 5);
    }
}
