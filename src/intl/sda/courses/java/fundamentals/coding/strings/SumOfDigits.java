package intl.sda.courses.java.fundamentals.coding.strings;

public class SumOfDigits {
    public static void main(String[] args) {
        int number1 = 48957;
        System.out.println("Sum of digits is: "+SumOfDigits.getSumOfDigitsVersion1(number1));
        System.out.println("Sum of digits is: "+SumOfDigits.getSumOfDigitsVersion2(number1));
    }
    public static int getSumOfDigitsVersion1(int number) {
        int sum = 0;

        while (number > 0) {
            int digit = (int) number % 10;
            sum += digit;
            number /= 10;
            System.out.println("Last digit: "+digit+" new number to ectract digits: "+number);
        }


        return sum;
    }
    public static int getSumOfDigitsVersion2(int number) {
        int sum = 0;
        // using string concatenation to get the string representation of the number
        String numberAsString = "" + number;

        for (int i = 0; i < numberAsString.length();i++){
            int digit =Integer.parseInt(numberAsString.charAt(i)+"");
            sum += digit;
            //System.out.println(numberAsString.charAt(i));
        }
        return sum;
    }
}
