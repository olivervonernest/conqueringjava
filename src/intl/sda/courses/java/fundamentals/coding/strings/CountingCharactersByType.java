package intl.sda.courses.java.fundamentals.coding.strings;

public class CountingCharactersByType {

    public static void count(String s) {
         int letters = 0;
         int spaces = 0;
         int numbers = 0;
         int others = 0;

         for (int i = 0; i < s.length(); i++) {
             char c = s.charAt(i);
             if (('a' <= c && c <= 'z') || ('A' <= c && c <= 'Z')) {
                 letters++;
             }else if (c == ' ') {
                 spaces++;
             }else if ('0' <= c && c <= '9') {
                 numbers++;
             }else {
                 others++;
             }
         }
        System.out.println(letters + " letters, " + spaces+" spaces, "+ numbers + " numbers, "+others+" others.");
    }

    public static void main(String[] args) {
        String text = "Aa kiu, I swd skieo 2387. GH kiu: sieo?? 25.33";
        CountingCharactersByType.count(text);

    }

}
