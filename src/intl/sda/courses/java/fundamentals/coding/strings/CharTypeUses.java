package intl.sda.courses.java.fundamentals.coding.strings;

public class CharTypeUses {
    public static void main(String[] args) {
        int sum = 0;
        String text = "";

        for (char i = 'a'; i <= 'z'; i++) {
            System.out.printf("%s %d %n", i, (int) i);
            sum += i;
            text += i;
        }
        System.out.println("Sum of codes: " + sum);
        System.out.println("Characters concatenated into a string: " + text);

        char surprise = 10084; //Heart character
        System.out.println(surprise);
    }
}
