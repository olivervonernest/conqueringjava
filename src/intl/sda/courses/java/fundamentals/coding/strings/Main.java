package intl.sda.courses.java.fundamentals.coding.strings;

public class Main {
    public static void main(String[] args) {
        String word = "oliver";
        System.out.println(word + " -> " + StringUtils.reverseString(word));
    }
}
