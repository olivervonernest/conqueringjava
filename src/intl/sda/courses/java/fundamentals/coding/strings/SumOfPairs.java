package intl.sda.courses.java.fundamentals.coding.strings;

public class SumOfPairs {

    public static void showPairs(int[] arr, int sum) {
        for (int i = 0; i < arr.length - 1; i++) {
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[i] + arr[j] == sum) {
                    System.out.print(arr[i] + "+" + arr[j] + " ");
                }
            }
        }
        System.out.println();
    }

    public static void main(String[] args) {
        int[] numbers1 = {1, 2, 7, 3, 10, 2, 9};
        int sum1 = 4;
        SumOfPairs.showPairs(numbers1, sum1);
        int[] numbers2 = {3, 8, 1, 6, 2, 0, 5, 2, 7};
        int sum2 = 15;
        SumOfPairs.showPairs(numbers2, sum2);

    }
}
