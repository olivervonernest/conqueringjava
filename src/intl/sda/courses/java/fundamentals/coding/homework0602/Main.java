package intl.sda.courses.java.fundamentals.coding.homework0602;

public class Main {
    public static void main(String[] args) {
        /*
         *Usage of the AnotherWayOfRectangle class in a real life scenario.
         * Let's assume we need to calculate how many square meters of carpet we need to buy
         * to cover the floor for 3 rectangle shaped rooms.
         * 1st room: 3x4m
         * 2nd room: 4x6m
         * 3rd room: 2x3m
         */

        AnotherWayOfRectangle room1 = new AnotherWayOfRectangle(3, 4);
        AnotherWayOfRectangle room2 = new AnotherWayOfRectangle(4, 6);
        AnotherWayOfRectangle room3 = new AnotherWayOfRectangle(2, 3);

        room1.printRectangle();
        room2.printRectangle();
        room3.printRectangle();

        int totalArea = room1.calculateArea() + room2.calculateArea() + room3.calculateArea();
        System.out.println("We need " + totalArea + " m2 of carpet");

    }
}
