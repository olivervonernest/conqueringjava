package intl.sda.courses.java.fundamentals.coding.homework0602;

/**
 * Rectangle implemented using olnly static fields and methods.
 * THIS IS ONLY FOR TEACHING PUPROSES,
 * to see the differences between static and non-static fields and methods
 * RECTANGLE CLASS SHOULD BE IMPLEMENTED WITH NON-STATIC FIELDS AND METHODS
 */
public class RectangleStatic {
    private static int width;
    private static int height;

    public static int getWidth() {
        return width;
    }

    public static int getHeight() {
        return height;
    }

    public static void setWidth(int width) {
        if (width > 0) {
            RectangleStatic.width = width;
        }else {
            System.out.println("Width should be greater than 0");
        }

    }

    public static void setHeight(int height) {
        if (height > 0) {
            RectangleStatic.height = height;
        }else {
            System.out.println("Height should be greater than 0");
        }


    }

    public static int calculatePerimeter() {
        return 2 * RectangleStatic.width + 2 * RectangleStatic.height;

    }
    public static int calculateArea() {
        return RectangleStatic.width * RectangleStatic.height;
    }
}
