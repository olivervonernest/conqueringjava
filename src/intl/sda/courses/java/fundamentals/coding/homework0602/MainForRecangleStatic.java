package intl.sda.courses.java.fundamentals.coding.homework0602;

/**
 * Rectangle implemented using olnly static fields and methods.
 * THIS IS ONLY FOR TEACHING PUPROSES,
 * to see the differences between static and non-static fields and methods
 * RECTANGLE CLASS SHOULD BE IMPLEMENTED WITH NON-STATIC FIELDS AND METHODS
 */
public class MainForRecangleStatic {
    public static void main(String[] args) {
        /*
         *Usage of the AnotherWayOfRectangle class in a real life scenario.
         * Let's assume we need to calculate how many square meters of carpet we need to buy
         * to cover the floor for 3 rectangle shaped rooms.
         * 1st room: 3x4m
         * 2nd room: 4x6m
         * 3rd room: 2x3m
         */
        int totalArea = 0;
        // for 1st room
        RectangleStatic.setWidth(3);
        RectangleStatic.setHeight(4);
        totalArea += RectangleStatic.calculateArea();

        //For 2nd room

        RectangleStatic.setWidth(4);
        RectangleStatic.setHeight(6);
        totalArea += RectangleStatic.calculateArea();

        //For 3rd room

        RectangleStatic.setWidth(2);
        RectangleStatic.setHeight(3);
        totalArea += RectangleStatic.calculateArea();

        System.out.println("We need "+ totalArea + "m2 carpet");

    }
}
