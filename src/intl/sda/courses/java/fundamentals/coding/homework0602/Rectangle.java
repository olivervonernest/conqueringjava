package intl.sda.courses.java.fundamentals.coding.homework0602;

/*
Write class Rectangle with fields width and height of type int.
Generate constructor, getters and setters.
Write a method that calculates the perimeter of the rectangle
and a method that calculates the area of the rectangle.
Note: fields and methods are non-static.
 */
public class Rectangle {
    private int width;
    private int height;

    public int perimeterOfRectangle() {
        int sumPerimeter = (2 * width) + (2 * height);
        return sumPerimeter;
    }

    public int areaOfRectangle() {
        int sumArea = width * height;
        return sumArea;
    }

    public Rectangle(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        if (height > 0) {
            this.width = width;
        }else {
            System.out.println("Height can't be 0 or negative");
        }

    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        if (height > 0) {
            this.height = height;
        }else {
            System.out.println("Height can't be 0 or negative");
        }

    }


}
