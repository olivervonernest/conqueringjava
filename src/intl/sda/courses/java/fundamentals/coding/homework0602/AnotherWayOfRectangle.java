package intl.sda.courses.java.fundamentals.coding.homework0602;

public class AnotherWayOfRectangle {
    private int height;
    private int width;

    public AnotherWayOfRectangle(int width, int height) {
        setHeight(height);
        setWidth(width);
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        if (height > 0) {
            this.height = height;
        } else {
            System.out.println("Height should be greater than 0");
        }

    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        if (width > 0) {
            this.width = width;
        } else {
            System.out.println("Width should be greater than 0");
        }

    }

    public int calculatePerimeter() {
        return 2 * width + 2 * height;
    }

    public int calculateArea() {
        return width * height;
    }

    public void printRectangle() {
        System.out.println("Rectangle " + width + "x" + height + " perimeter is " + calculatePerimeter() + " and area is " + calculateArea());
    }
}
