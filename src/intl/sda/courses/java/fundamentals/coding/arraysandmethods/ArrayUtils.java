package intl.sda.courses.java.fundamentals.coding.arraysandmethods;

/*
Create class ArrayUtils.
It will contain a method that will determine the maximum value in an array of int,
a method that will determine the minimum value in an array of int,
a method that will calculate the sum of values in an array of int,
a method that will calculate the average of values in an array of int,
a method that will sort ascending an array of int and will return the sorted array.
 */
public class ArrayUtils {
    public static int maxValue(int[] arr) {
        int max = Integer.MIN_VALUE;
        for (int i = 0; i < arr.length; i++) {
            if (max < arr[i]) {
                max = arr[i];
            }
        }
        return max;
    }

    public static int minValue(int[] arr) {
        int min = Integer.MAX_VALUE;
        for (int i = 0; i < arr.length; i++) {
            if (min > arr[i]) {
                min = arr[i];
            }
        }
        return min;
    }

    public static int sumValue(int[] arr) {
        int sum = 0;
        for (int i = 0; i < arr.length; i++) {
            sum = sum + arr[i];
        }
        return sum;
    }

    public static double avgValue(int[] arr) {
        return (double) sumValue(arr) / arr.length;
    }

    public static int[] sort(int[] arr) {
        // bubble sort
        // the wall decreases at each step by 1
        for (int wall = arr.length; wall > 0; wall--) {
            for (int i = 0; i < wall-1; i++) {
                // comparing each element to the next one
                // j from the video is actually i+1
                if (arr[i] > arr[i+1]) {
                    // switching the values if the elements are not in ascending order
                    int temp = arr[i]; // to switch 2 values we need a temporary variable to store one of the values
                    arr[i] = arr[i+1];
                    arr[i+1] = temp;
                }
            }
        }
        return arr;
    }

    public static String printArray(int[] arr) {
        String arrayToPrint = "";
        for (int i = 0; i < arr.length; i++) {
            arrayToPrint += arr[i] + " ";
        }
        return arrayToPrint;
    }



}
