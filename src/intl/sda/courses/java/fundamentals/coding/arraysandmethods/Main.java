package intl.sda.courses.java.fundamentals.coding.arraysandmethods;
/*
Create class ArrayUtils.
It will contain a method that will determine the maximum value in an array of int,
a method that will determine the minimum value in an array of int,
a method that will calculate the sum of values in an array of int,
a method that will calculate the average of values in an array of int,
a method that will sort ascending a an array of int and will return the sorted array.
 */
public class Main {
    public static void main(String[] args) {
        int[] arr1 = {4, 3, 6, 0, -8, 35, 43, 1, 44, 78};
        System.out.println(ArrayUtils.maxValue(arr1));
        System.out.println(ArrayUtils.minValue(arr1));
        System.out.println(ArrayUtils.sumValue(arr1));
        System.out.println(ArrayUtils.avgValue(arr1));
        System.out.println(ArrayUtils.printArray(ArrayUtils.sort(arr1)));
    }
}
