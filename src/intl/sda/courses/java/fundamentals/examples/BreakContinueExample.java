package intl.sda.courses.java.fundamentals.examples;

import java.util.Scanner;

public class BreakContinueExample {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.print("n=");
        int n = scanner.nextInt();
        System.out.println("Counting 1 to 10 (using break statement");
        System.out.println("Stop before "+n);
        for (int i = 1; i <= 10; i++) {
            if (i==n) {
                break;
            }
            System.out.println(i);
        }
        System.out.println("Counting 1 to 10 (using continue statement)");
        System.out.println("Skip "+n);
        for (int i = 1; i <= 10; i++) {
            if (i == n) {
                continue;
            }
            System.out.println(i);
        }

    }
}
