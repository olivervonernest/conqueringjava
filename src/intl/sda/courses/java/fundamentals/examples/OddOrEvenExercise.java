package intl.sda.courses.java.fundamentals.examples;

import java.util.Scanner;

public class OddOrEvenExercise {
    public static void main(String[] args) {
        Scanner scanner1 = new Scanner(System.in);
        // Read an integer number from the keyboard. Print an appropriate message if the number is odd or even.
        int nm;
        System.out.println("Is this odd or even? Insert a number!");
        nm = scanner1.nextInt();

        if (nm % 2 == 0){
            System.out.println("It is even");
        }else {
            System.out.println("It is odd");
        }

    }
}
