package intl.sda.courses.java.fundamentals.examples;

import java.util.Scanner;

import static java.lang.Integer.MAX_VALUE;

public class HomeWork100121 {
    public static void main(String[] args) {

    }

}

class ex1 {
    public static void main(String[] args) {
        //Implement the program displaying all the numbers from the range 1 - 100
        // which are divisible by 5 beginning from 100 (in reverse order).
        for (int i = 100; i >= 1; i -= 5) {
            System.out.println(i);
        }
    }
}

class ex2 {
    public static void main(String[] args) {
        //Read int n form the keyboard (n<20).
        //Write a program that prints n lines with 1 * for line 1,
        // ** for line 2, *** for line 3, n * for line n.
        Scanner scanner = new Scanner(System.in);
        System.out.println("Insert a number between 1-20");
        int i, j, n;
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter n: ");
        n = sc.nextInt();
        if (n < 20 && n > 0) {
            for (i = 0; i < n; i++) {
                for (j = 0; j <= i; j++) {
                    System.out.print("*");
                }
                System.out.println();
            }
        } else {
            System.out.println("Number not allowed");
        }
        /*

        int input = 0;
        int counter = 1;
        Scanner scanner = new Scanner(System.in);
        do {
            System.out.println("Type a number between 1 - 19 (incl)");
            input = scanner.nextInt();
        }
        while (input < 1 || input > 19);
        while (input >= counter) {
            for (int i = 0; i < counter ; i++) {
                System.out.print("*");
            }
            System.out.println("");
            counter++;
        }

         */

    }
}

class ex3 {
    public static void main(String[] args) {
        // FizzBuzz problem.
        // Write a program that reads an integer
        // prints fizz if the number is divisible by 3,
        // prints buzz if the number is divisible by 5 and
        // prints fizzbuzz if the number is divisible by 15.
        System.out.println("Input a number to fizzbuzz on!");
        Scanner scanner = new Scanner(System.in);
        int i = scanner.nextInt();
        if (i % 3 == 0) {
            System.out.print("Fizz");
        }
        if (i % 5 == 0) {
            System.out.println("Buzz");
        }
        /*

        Scanner scanner = new Scanner(System.in);
        System.out.println("FIZZBUZZ");
        System.out.println("Please enter any number: ");
        int nm = scanner.nextInt();
        //What if the number is divisible by all, like 15 or 30, it suits for all..is this a right solution for that?
        if (nm % 15 == 0) {
            System.out.println("FizzBuzz");
        } else if (nm % 5 == 0) {
            System.out.println("Buzz");
        } else if (nm % 3 == 0) {
            System.out.println("Fizz");
        }

         */
    }
}

class ex4 {
    public static void main(String[] args) {
        /*
        Calculator.
        Read int a and int b from the keyboard.
        Display a menu that allows user to choose an operation to perform
        1) Calculate a+b.
        2) Calculate a-b
        3) Calculate a*b
        4) Calculate a/b
        5) Exit.
        Let the user input the operation (from 1 to 5),
        display the result and then the menu until the user chooses 5.
         */
        Scanner scanner = new Scanner(System.in);
        float a;
        float b;
        float z = 0;
        System.out.print("a=");
        a = scanner.nextFloat();
        System.out.print("b=");
        b = scanner.nextFloat();
        do {
            System.out.println("Choose the calculation operation by selecting number 1 to 5:\n " +
                    "1. Calculate a+b \n 2. Calculate a-b \n 3. Calculate a*b \n 4. Calculate a/b \n 5. EXIT calculation \n\n Choose your number: \n");
            z = scanner.nextFloat();
            if (z == 1) {
                System.out.println("a + b= " + (a + b));
            } else if (z == 2) {
                System.out.println("a - b= " + (a - b));
            } else if (z == 3) {
                System.out.println("a * b= " + (a * b));
            } else if (z == 4) {
                System.out.printf("a / b=%.2f%n ", (a / b));
            } else {
                System.out.println("Calculation completed");
            }
        } while (z != 5);

        /*
        int nm1 = 0, nm2 = 0, ch1, ch2;
        do {
            Scanner scanner = new Scanner(System.in);

            System.out.println(" 1) Calculate a+b \n 2) Calculate a-b \n 3) Calculate a*b \n 4) Calculate a/b \n 5) Exit \n Choose an operation to perform:");
            ch1 = scanner.nextInt();
            if (ch1 < 5) {
                System.out.println("Insert a:");
                nm1 = scanner.nextInt();
                System.out.println("Insert b:");
                nm2 = scanner.nextInt();
            } else
                break;
            switch (ch1) {
                case 1:
                    System.out.println(nm1 + " + " + nm2 + " = " + (nm1 + nm2));
                    break;
                case 2:
                    System.out.println(nm1 + " - " + nm2 + " = " + (nm1 - nm2));
                    break;
                case 3:
                    System.out.println(nm1 + " * " + nm2 + " = " + (nm1 * nm2));
                case 4:
                    if (nm2 == 0) {
                        System.out.println("Cannot divide by 0");
                    } else {
                        System.out.println(nm1 + " / " + nm2 + " = " + (nm1 / nm2) + " and remainder is " + (nm1 % nm2));
                    }
                    break;
                case 5:
                    break;
                default:
                    System.out.println("Invalid choice");
            }

            System.out.println("Continue? Yes=1, No=2");
            ch2 = scanner.nextInt();
        } while (ch2 == 1);

         */

    }

}
