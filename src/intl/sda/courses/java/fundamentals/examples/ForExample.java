package intl.sda.courses.java.fundamentals.examples;

import java.util.Scanner;

public class ForExample {
    public static void main(String[] args) {
        /*
        // prints Hello World 10 times
        for (int i = 0; i < 10; i++) {
            System.out.println("Hello world!");
        }

        // prints number from 1 to 10
        for (int i = 1; i <= 10; i++) {
            System.out.println(i);
        }

        // prints number from 10 to 0
        for (int i = 10; i >= 0; i--) {
            System.out.println(i);
        }
        */

        //Read an integer from the keyboard. Print all odd numbers smaller then it.
        Scanner scanner = new Scanner(System.in);
        System.out.println("Insert number");
        int n = scanner.nextInt();
        /*
        // solution 1
        for (int i = 1; i < n; i++) {
            if (i % 2 == 1) {
                System.out.println(i);
            }
        }

         */

        // solution 2
        for (int i = 1; i < n; i += 2) {
            System.out.println(i);
        }

    }



}
