package intl.sda.courses.java.fundamentals.examples;

import java.util.Scanner;

// Read an array of String using while and print it using do-while (instead of using for).
public class HomeWork160121Ex3 {
    public static void main(String[] args) {
        int n;
        int i = 0;
        int j = 0;
        Scanner scanner = new Scanner(System.in);
        System.out.print("Number of elements = ");
        n = scanner.nextInt();
        String[] arr = new String[n];
        while (i < n) {
            System.out.print("Input element " + i + " = ");
            arr[i] = scanner.next();
            i++;
        }
        do {
            System.out.println(arr[j]);
            j++;
        } while (j < arr.length);



    }
}
