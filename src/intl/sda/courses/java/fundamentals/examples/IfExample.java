package intl.sda.courses.java.fundamentals.examples;

public class IfExample {

    public static void main(String[] args) {
        /*

        float temperature = 41f;

        //Temperature below 34C is hypotermia
        //Temperature between 37C and 41C is fever
        //Temperature below 20C and 41C is not valid

        if (temperature < 20 || temperature > 41) {
            System.out.println("Temperature is not valid!");
        } else if (temperature >= 34 && temperature <=37) {
            System.out.println("Your body temperature is normal");
        } else if (temperature >37) {
            System.out.println("You have a fever");
        } else {
            System.out.println("You are hypotermic!");
        }

         */

        // ülesanne: values 1-10. kui alla 5(You are failed!), kui 5-7 vahel(you are passed), kui üle 7(you did great)

        int grade = 4;

        if (grade < 1 || grade > 10) {
            System.out.println("Invalid grade");
        } else if (grade < 5) {
            System.out.println("You are failed!");
        } else if (grade >= 5 && grade < 7) {
            System.out.println("You passed!");
        } else {
            System.out.println("You did great!");
        }


    }


}
