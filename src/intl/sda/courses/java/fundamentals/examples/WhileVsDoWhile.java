package intl.sda.courses.java.fundamentals.examples;

import java.util.Scanner;

public class WhileVsDoWhile {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        // HINT: test for value 3 and 0 to see difference between while and do-while
        System.out.println("How many times you want me to write Hello World?");
        int n = scanner.nextInt();
        System.out.println("While:");
        int i = 0;
        while (i < n) {
            System.out.println("Hello World");
            i++;
        }

        System.out.println("Do-While:");
        int j = 0;
        do {
            System.out.println("Hello World");
            j++;
        } while (j < n);
    }
}
