package intl.sda.courses.java.fundamentals.examples;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexExamples {
    public static void main(String[] args) {
        // regex to test if a String is a name:
        // it has to start with capital letter
        // that will be followed by one or more small letters
        String name = "Li";
        Pattern pattern = Pattern.compile("[A-Z][a-z]+");
        Matcher matcher = pattern.matcher("Li");
        System.out.println(name + " is a name?" + matcher.matches());

        name = "li";
        matcher = pattern.matcher(name);
        System.out.println(name + " is a name?" + matcher.matches());

        name = "Victor";
        matcher = pattern.matcher(name);
        System.out.println(name + " is a name?" + matcher.matches());

        name = "VIctor";
        matcher = pattern.matcher(name);
        System.out.println(name + " is a name?" + matcher.matches());

        name = "vIctor";
        matcher = pattern.matcher(name);
        System.out.println(name + " is a name?" + matcher.matches());

        name = "Victor2";
        matcher = pattern.matcher(name);
        System.out.println(name + " is a name?" + matcher.matches());

        // regex = a*b
        // a string that can start with 0 or more occurences of letter followed by 1 letter b
        pattern = Pattern.compile("a*b");
        String text = "ab";
        matcher = pattern.matcher(text);
        System.out.println(text + " matches pattern a*b ?" + matcher.matches());

        text = "b";
        matcher = pattern.matcher(text);
        System.out.println(text + " matches pattern a*b ?" + matcher.matches());

        text = "aaaaaab";
        matcher = pattern.matcher(text);
        System.out.println(text + " matches pattern a*b ?" + matcher.matches());

        text = "aaaaaabb";
        matcher = pattern.matcher(text);
        System.out.println(text + " matches pattern a*b ?" + matcher.matches());

        // regex a?b
        // 0 or 1 occurence
        pattern = Pattern.compile("a?b");
        text = "aab";
        matcher = pattern.matcher(text);
        System.out.println(text + " matches pattern 'a?b' ? " + matcher.matches());
        pattern = Pattern.compile("a?b");
        text = "ab";
        matcher = pattern.matcher(text);
        System.out.println(text + " matches pattern 'a?b' ? " + matcher.matches());

        // regex a{n,m}b
        // at least n and maximum of m occurences

        pattern = Pattern.compile("a{1,4}b");
        String text1 = "ab";
        matcher = pattern.matcher(text1);
        System.out.println(text1 + " matches pattern a{1,4}b ?" + matcher.matches());
        text1 = "aab";
        matcher = pattern.matcher(text1);
        System.out.println(text1 + " matches pattern a{1,4}b ?" + matcher.matches());
        text1 = "aaaabb";
        matcher = pattern.matcher(text1);
        System.out.println(text1 + " matches pattern a{1,4}b?" + matcher.matches());
        text1 = "aaabb";
        matcher = pattern.matcher(text1);
        System.out.println(text1 + " matches pattern a{1,4}b ?" + matcher.matches());

        // regex a{n,}b
        //at least n occurances

        Pattern leastN=Pattern.compile("a{3,}b");
        String Exp4_1="aaab";
        String Exp4_2="aaaab";
        String Exp4_3="ab";
        matcher=leastN.matcher(Exp4_1);
        System.out.println(Exp4_1+ " matches pattern a{3,}b? " + matcher.matches());
        matcher=leastN.matcher(Exp4_2);
        System.out.println(Exp4_2+ " matches pattern a{3,}b? " + matcher.matches());
        matcher=leastN.matcher(Exp4_3);
        System.out.println(Exp4_3+ " matches pattern a{3,}b? " + matcher.matches());

        //regex a{n}b
        // Exactly n occurances

        pattern = Pattern.compile("j{4}r*");
        String ex_five = "jjjjr";
        matcher = pattern.matcher(ex_five);
        System.out.println(ex_five + " matches pattern j{4}r* ?" + matcher.matches());
        ex_five = "jjjj";
        matcher = pattern.matcher(ex_five);
        System.out.println(ex_five + " matches pattern j{4}r* ?" + matcher.matches());
        ex_five = "jr";
        matcher = pattern.matcher(ex_five);
        System.out.println(ex_five + " matches pattern j{4}r* ?" + matcher.matches());
        ex_five = "jjjjjjrr";
        matcher = pattern.matcher(ex_five);
        System.out.println(ex_five + " matches pattern j{4}r* ?" + matcher.matches());

        // https://docs.oracle.com/javase/8/docs/api/java/util/regex/Pattern.html


    }
}
