package intl.sda.courses.java.fundamentals.examples;

import java.util.Scanner;

public class LeapOrNotExercise {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        // Read an integer number form the keyboard representing a year.
        // Print an appropriate message if the year is a leap year or not.
        // Document yourself on what are the conditions for the leap year.

        System.out.println("Is it leap year or not? Insert any year: ");
        int year = scanner.nextInt();

        //If the year is divisible by century, then not leap year
        //If divisible by 4, then leap year
        //If divisible by 400, then leap year

        if ((year % 400 == 0) || ((year % 4 == 0) && (year % 100 != 0))) {
            System.out.println("Year " + year + " is a leap year");
        } else {
            System.out.println("Year " + year + " is not a leap year");
        }
    }
}
