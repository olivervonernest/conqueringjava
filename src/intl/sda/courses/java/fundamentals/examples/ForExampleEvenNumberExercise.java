package intl.sda.courses.java.fundamentals.examples;

import java.util.Scanner;
    //print all even numbers
public class ForExampleEvenNumberExercise {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Insert number");
        int n = scanner.nextInt();
        /*
        solution 1
        for (int i = 2; i < n; i += 2) {
            System.out.println(i);
        }

         */

        //solution 2
        for (int i = 1; i < n; i++) {
            if (i % 2 == 0) {
                System.out.println(i);
            }
        }
    }
}
