package intl.sda.courses.java.fundamentals.examples;

//Using the MethodsExample , write a method that calculate the maximum value for an array of int.
public class HomeWork160121Ex1 {
    public static int maxValue(int[] arr) {
        int maximumValue = arr[0]; //0 näitab asukohta arrays. ja 0 on esimene asukoht arrrayl
        for (int i = 0; i < arr.length; i++) {
            //kontrollib asukohal olevaid numbreid. kui eemaldan kommentaarid, siis näitab kuidas ta seda teeb.
//            System.out.println("current maximumValue = "+maximumValue
//                    +" current i = "+i
//                    +" current arr[i]= "+ arr[i]);
            if (arr[i] > maximumValue) {
                maximumValue = arr[i]; // kui sellel kohal on suurim number, siis maximumValue = asukoht sellel arrayl
//                System.out.println("maximumValue changed to " + maximumValue);
            }
        }
        return maximumValue;
    }

    public static void main(String[] args) {
        int[] myArray = {1, 2, 3, 4, 5, 6};
        System.out.println("Maximum value is " + maxValue(myArray));
        /*
        int[] mySecArray = {324324, 2343242, 432432445, 234324324, 34324324};
        System.out.println("Maximum value of second array is "+maxValue(mySecArray));

         */

    }
}
