package intl.sda.courses.java.fundamentals.examples;

import java.util.Random;
import java.util.Scanner;

public class GuessTheNumber {
    public static void main(String[] args) {
        Random random = new Random();
        //random.nextInt(100) randomly generates a number between 0 and 100, excluding 100
        int noToGuess = random.nextInt(100)+1;

        //System.out.println(noToGuess);

        int n;
        Scanner scanner = new Scanner(System.in);
        do {
            System.out.println("What is the number? (1-100");
            n = scanner.nextInt();
            if (n == noToGuess) {
                System.out.println("Congratulations! You guessed the number!");
            } else if (n < noToGuess) {
                System.out.println("Your number is too small. Try again!");
            } else {
                System.out.println("Your number is too large. Try again!");
            }
        } while (n != noToGuess);
    }
}
