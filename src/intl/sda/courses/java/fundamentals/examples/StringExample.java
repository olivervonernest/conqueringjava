package intl.sda.courses.java.fundamentals.examples;

import java.util.Locale;

public class StringExample {
    public static void main(String[] args) {
        String s1 = "Tom";
        String s2 = "Tom";
        String s3 = new String("Jerry");
        String s4 = new String("Jerry");

        //prints true because s1 and s2 values are stored in String Pool
        System.out.println(s1 == s2);

        // prints false because s3 and s4 values are stored in Heap in different memory areas
        //and == operator compares memory addresses
        System.out.println(s3 == s4);

        //prints true because it compares the values contained by s3 and s4
        System.out.println(s3.equals(s4));

        String s5 = "This is a text.";

        System.out.println("'" + s5 + "' has " + s5.length() + " characters.");

        System.out.println(s5.toUpperCase(Locale.ROOT));

        System.out.println(s5.toLowerCase(Locale.ROOT));

        System.out.println("String 'is' is found at the position " + s5.indexOf("is"));

        System.out.println("String ' is ' is found at the position " + s5.indexOf(" is "));

        System.out.println(s5.replaceFirst("is", "at"));

        System.out.println(s5);

        System.out.println(s5.replaceAll("is", "at"));
    }
}

