package intl.sda.courses.java.fundamentals.examples;

import java.util.Scanner;

public class OperatorsExample {
    public static void main(String[] args) {
        /*
        //assignment operators examples
        int a = 10;

        System.out.println("a=" + a);

        a += 5; // a = a + 5;
        System.out.println("a=" + a);

        a -= 3; // a = a - 3;
        System.out.println("a=" + a);

        a *= 6; // a = a * 3;
        System.out.println("a=" + a);

        a /= 2; // a = a / 2;
        System.out.println("a=" + a);

        // arithmetic operators examples
        Scanner scanner = new Scanner(System.in);

        int x, y;
        System.out.println("x=");
        x = scanner.nextInt(); // read the next integer value from the keyboard
        System.out.println("y=");
        y = scanner.nextInt();
        System.out.println("x+y=" + (x + y));
        System.out.println("x-y=" + (x - y));
        System.out.println("x/y=" + (x / y));
        System.out.println("x%y=" + (x % y));


        // incrementation operators example
        int b = 9;
        System.out.println("b++ value is "+ (b++)); // post-incrementation
        System.out.println("value is now " + b);
        System.out.println("++b value is " + (++b)); // pre-incrementation

        // decrementation operators example
        int c = 10;
        System.out.println("c-- value is "+ (c--)); // post-decrementation: print value of c and THEN decrease value of by c 1
        System.out.println("value is now " + c);
        System.out.println("--c value is " + (--c)); // pre-decrementation: decrease value of c by 1 and THEN print it



        // relational operators example
        int v1 = 12;
        int v2 = 13;
        System.out.println("v1=" + v1 + " v2=" + v2);
        System.out.println("Values are equal? ");
        System.out.println(v1 == v2);
        System.out.println("Values are different? ");
        System.out.println(v1 != v2);
        System.out.println("v1 is greater than v2 ");
        System.out.println(v1 > v2);
        System.out.println("v1 is smaller than v2 ");
        System.out.println(v1 < v2);

         */

        // logical operators example
        int age = 16;
        System.out.println("Is the age valid (age>0 and age <100)? ");
        System.out.println(age > 0 && age <100);

        int age2 = 33;
        System.out.println("Is the age not a working age (working age must be between 18 and 65)? ");
        System.out.println(age2<=18 || age2>=65);


    }
}
