package intl.sda.courses.java.fundamentals.examples;

public class HomeWork160121Ex2 {
    public static void main(String[] args) {
        //Write a class that calculates the sum on each row and the sum on each column for this matrix:
        //3 5 4 6
        //6 7 8 1
        //2 4 7 9
        int[][] matrix = {{3, 5, 4, 6}, {6, 7, 8, 1}, {2, 4, 7, 9}};

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println();
        }
        for (int i = 0; i < matrix.length; i++) {
            int sumOnRow = 0;
            for (int j = 0; j < matrix[i].length; j++) {
                sumOnRow += matrix[i][j];
            }
            System.out.println("Sum on row " + i + " is " + sumOnRow);
        }
        for (int j = 0; j < matrix[0].length; j++) {
            int sumOnColumn = 0;
            for (int i = 0; i < matrix.length; i++) {
                sumOnColumn += matrix[i][j];
            }
            System.out.println("Sum on column " + j + " is " + sumOnColumn);
        }

    }
}
