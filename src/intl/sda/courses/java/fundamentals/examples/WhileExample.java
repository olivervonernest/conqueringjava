package intl.sda.courses.java.fundamentals.examples;

public class WhileExample {
    public static void main(String[] args) {
        // print Hello World 10 times
        System.out.println("Print Hello World 10 times with while: ");
        int i = 0;
        while (i < 10) {
            System.out.println("Hello World");
            i++;
        }
        // print hello world 10 times with do-while method
        System.out.println("Print Hello World 10 times with do-while: ");
        int j = 0;
        do {
            System.out.println("Hello World");
            j++;
        } while (j < 10);

    }
}
