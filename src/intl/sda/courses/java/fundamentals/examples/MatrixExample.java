package intl.sda.courses.java.fundamentals.examples;

public class MatrixExample {
    public static void main(String[] args) {
        //initializing a 3 by 3 matrix
        // 3 5 7
        // 6 9 8
        // 5 7 2
        int[][] matrix = {{3, 5, 7}, {6, 9, 8}, {5, 7, 2}};

        for (int i = 0; i < matrix.length; i++) {
            // matrix[i].length is the number of elements/columns on line i
            for (int j = 0; j < matrix[i].length; j++) {
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println();
        }

        // calculating the sum of values on each row
        //iterating on rows
        for (int i = 0; i < matrix.length; i++) {
            //iterating on columns
            int sumOnRow = 0;
            for (int j = 0; j < matrix[i].length; j++) {
                sumOnRow += matrix[i][j];
            }
            System.out.println("Sum on row " + i + " is " + sumOnRow);
        }

        // calculating the sum of values on each column
        //iterating on columns
        // matrix[0].length is the number of columns on row 0
        for (int j = 0; j < matrix[0].length; j++) {
            //iterating on columns
            int sumOnColumn = 0;
            for (int i = 0; i < matrix.length; i++) {
                sumOnColumn += matrix[i][j];
            }
            System.out.println("Sum on column " + j + " is " + sumOnColumn);
        }
    }
}
