package intl.sda.courses.java.fundamentals.examples;

public class ArraysExamples {
    public static void main(String[] args) {
        // declaring an array of int with three elements
        // instantiate it (allocating memory space for the elements
        int[] arr1 = new int[3];
        // initializing the elements
        arr1[0] = 3;
        arr1[1] = 2;
        arr1[2] = 5;

        //iterating over an array and printing each element
        for (int i = 0; i < arr1.length; i++) {
            System.out.print(arr1[i] + " ");
        }

        // declaring an array of String with 5 elements and initializing it
        String[] arr2 = {"blue", "green", "white", "red", "orange"};

        // iterating over an array and printing each element
        for (int i = 0; i < arr2.length; i++) {
            System.out.print(arr2[i] + " ");
        }



    }
}
