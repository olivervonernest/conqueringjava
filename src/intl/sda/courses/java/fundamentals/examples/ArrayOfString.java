package intl.sda.courses.java.fundamentals.examples;

import java.util.Scanner;

public class ArrayOfString {
    public static void main(String[] args) {
        int n;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Number of elements");
        n = scanner.nextInt();
        System.out.println("input elements");
        String[] arr = new String[n];
        for (int i = 0; i < n; i++) {
            System.out.print("element "+i+"=");
            arr[i] = scanner.next();
        }
        System.out.println("Your array is ");
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i]+" ");
        }
    }
}
