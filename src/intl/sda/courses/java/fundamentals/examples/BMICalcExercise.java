package intl.sda.courses.java.fundamentals.examples;

import java.util.Scanner;

public class BMICalcExercise {
    public static void main(String[] args) {
        Scanner scanner1 = new Scanner(System.in);
        //Write a program that calculates your BMI based on height and weight read from the keyboard using Scanner.
        // Body Mass Index is a simple calculation using a person's height and weight.
        // The formula is BMI = kg/m2 where kg is a person's weight in kilograms and m2 is their height in meters squared.
        // A BMI of 25.0 or more is overweight, while the healthy range is 18.5 to 24.9.
        // Print appropriate messages for underweight, healthy and overweight (bonus points for the funniest messages!).
        double kg = 0.0f;
        double ht = 0.0f;
        System.out.println("Insert your weight in kg: ");
        kg = scanner1.nextDouble();
        System.out.println("Insert your height in meters ");
        ht = scanner1.nextDouble();
        double BMI = kg / (ht * ht);
        System.out.println("Your body mass index is " + BMI + " kg/m2");

        if (BMI >= 18.5f && BMI <= 24.9f) {
            System.out.println(" You are healthy!");
        } else if (BMI > 25.0f) {
            System.out.println(" You are overweighted");
        } else if (BMI < 18.5f) {
            System.out.println(" You are underweighted");
        }


    }
}
