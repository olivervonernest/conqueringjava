package intl.sda.courses.java.fundamentals.examples;

import java.util.Scanner;

public class SwitchExample {
    public static void main(String[] args) {
        /*
        Declare integer variable grade and give it a value.
        Valid values are from 1 to 10 inclusive.
        If grade is bellow 5 print "You failed!",
        if grade is between 5 and 7 inclusive print "You passed!",
        if grade is above 7 print "You did great!".
        Solve the exercise using switch statement.
         */
        /*
        int grade;
        Scanner scanner = new Scanner(System.in);
        System.out.print("Input your grade: ");
        grade = scanner.nextInt();

        switch (grade) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5: System.out.println("You failed!"); break;
            case 6:
            case 7: System.out.println("You passed!"); break;
            case 8:
            case 9:
            case 10:
                System.out.println("You did great!"); break;
            default:
                System.out.println("Invalid input!"); break;
        }

         */

        //Read an integer number from the keyboard ranging from 1 to 12.
        // Print the name of the corresponding month.
        // Print an appropriate message if the value is not in the range.
        int month;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter month number: ");
        month = scanner.nextInt();

        switch (month) {
            case 1:
                System.out.println("January");
                break;
            case 2:
                System.out.println("February");
                break;
            case 3:
                System.out.println("March");
                break;
            case 4:
                System.out.println("April");
                break;
            case 5:
                System.out.println("May");
                break;
            case 6:
                System.out.println("June");
                break;
            case 7:
                System.out.println("July");
                break;
            case 8:
                System.out.println("August");
                break;
            case 9:
                System.out.println("September");
                break;
            case 10:
                System.out.println("October");
                break;
            case 11:
                System.out.println("November");
                break;
            case 12:
                System.out.println("December");
                break;
            default:
                System.out.println("Wrong input");
                break;
        }


    }
}
