package intl.sda.courses.java.fundamentals.examples;

public class ConversionExample {
    public static void main(String[] args) {
        /*
        //partial loss of information when converting large ints to float
        int x = Integer.MAX_VALUE;
        float y = x;
        System.out.println("x= "+x);
        System.out.println("y= "+y); // y is printed in an exponential form
        System.out.printf("y=%.0f%n", y); // y printed in a formatted way, with 0 decimals

         */

        // cast example
        double a = 99.9989;
        int b = (int) a;
        System.out.println(a + " cast to int is " + b);

        long c = Math.round(a);
        System.out.println(a + " rounded to long is " + c);
    }
}
