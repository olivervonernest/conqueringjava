package intl.sda.courses.java.fundamentals.classes;
// Write a class Cat with fields String name, int age, String color, String pattern.
// Write getter and setters for the fields and implement data validation into the setters
// ((((( Name should not be null,
// default value should be "Some cat",
// age should be between 1 and 20,
// color should have one of the values white, black, tan, orange,
// pattern should have one of the values plain, tabby, spots. ))))))
// Define constructors Cat() -
// give default values for each field:
// Cat(String name, int age, String color, String pattern) and
// Cat(String name, int age) with default values for color: tan and pattern: tabby.
// Define printCat method that prints all the value of the fields.
// Write a CatApplication class that tests the use of Cat class.
// (See Dog and DogApplication classes for reference from today's training session).

public class Cat {
    private String name;
    private int age;
    private String color;
    private String pattern;

    public void setName(String name) {
        // Name should not be null,
        // default value should be "Some cat",
        if (name != null) {
            this.name = name;
        } else {
            this.name = "Some cat";
            System.out.println("Cat name can't be null");
        }

    }

    public void setAge(int age) {
        // age should be between 1 and 20,
        if (age >= 1 && age <= 20) {
            this.age = age;
        } else {
            this.age = 0;
            System.out.println("Cat age must be between 1-20");
        }

    }

    public void setColor(String color) {
        // color should have one of the values white, black, tan, orange,
        if (color.equals("white") || color.equals("black") || color.equals("tan") || color.equals("orange")) {
            this.color = color;
        } else {
            System.out.println("Color must be white, black, tan or orange");
        }

    }

    public void setPattern(String pattern) {
        // pattern should have one of the values plain, tabby, spots.
        if (pattern.equals("plain") || pattern.equals("tabby") || pattern.equals("spots")) {
            this.pattern = pattern;
        } else {
            System.out.println("Pattern must be plain, tabby or spots");
        }
    }

    // Define constructors Cat() -
    // give default values for each field:
    // Cat(String name, int age, String color, String pattern) and
    // Cat(String name, int age) with default values for color: tan and pattern: tabby.


    public Cat() {
        this.setName("Some cat");
        this.setAge(0);
        this.setColor("tan");
        this.setPattern("tabby");
    }

    public Cat(String name, int age, String color, String pattern) {
        this.setName(name);
        this.setAge(age);
        this.setColor(color);
        this.setPattern(pattern);
    }

    public Cat(String name, int age) {
        this();
        this.setName(name);
        this.setAge(age);
    }

    // Define printCat method that prints all the value of the fields.

    public void printCat() {
        System.out.println(this.name + " " + this.age + " years " + this.color + " " + this.pattern);
    }
}
