package intl.sda.courses.java.fundamentals.classes;

public class Dog {
    private String name;
    private String size; // size should be small, medium, large
    private int age;
    private String breed;

    //rewriting using the default constructor to provide default values for the fields
    public Dog() {
        this.setName("Dog's name");
        this.setSize("medium");
        this.setAge(1);
        this.setBreed("mixed breed");
    }

    public Dog(String name, int age) {
        this();
        this.setName(name);
        this.setAge(age);
    }

    public Dog(String name, String size, int age, String breed) {
        this.setName(name);
        this.setSize(size);
        this.setAge(age);
        this.setBreed(breed);
    }

    public void setName(String name) {
        if (name != null) {
            this.name = name;
        } else {
            this.name = "Dog's name";
            System.out.println("Name of dog cant be null");
        }

    }

    public void setSize(String size) {
        if (size.equals("small") || size.equals("medium") || size.equals("large")) {
            this.size = size;
        } else {
            this.size = "medium";
            System.out.println("Dog size must be small or medium or large!");
        }
    }

    public void setAge(int age) {
        if (age >= 1 && age <= 30) {
            this.age = age;
        } else {
            this.age = 1; // default value for age
            System.out.println("Dog age must be between 1 - 30");
        }

    }

    public void setBreed(String breed) {
        if (breed != null){
            this.breed = breed;
        }else {
            this.breed = "mixed breed"; // default value for breed
            System.out.println("Dog breed cant be null");
        }

    }

    public void printDog() {
        System.out.println(this.name + " "
                + this.age + " years "
                + this.size + " size "
                + this.breed);
    }
}
