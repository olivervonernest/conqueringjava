package intl.sda.courses.java.fundamentals.classes;

public class CarApplication {
    public static void main(String[] args) {
        Car car1 = new Car();
        car1.setBrand("Mercedes");
        car1.setColor("red");
        car1.setMaxSpeed(250);
        car1.printCarParameters();

        Car car2 = new Car();
        car2.setBrand("Audi");
        car2.setColor("grey");
        car2.setMaxSpeed(200);
        car2.printCarParameters();


    }
}
