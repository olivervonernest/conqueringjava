package intl.sda.courses.java.fundamentals.classes;

public class DogApplication {
    public static void main(String[] args) {
        Dog myDog = new Dog("max","bigdog",200,"mix breed");
        myDog.printDog();

        Dog dog1 = new Dog();
        dog1.printDog();

        Dog dog2 = new Dog("Maia",5);
        dog2.printDog();
    }
}
