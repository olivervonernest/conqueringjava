Anna, Johnson, 1985-02-15, UK, London, 34 Lillies St
Anna, Johnson, 1997-01-05, UK, Liverpool, 24 Portside St
Beatrice, Noll, 1994-10-05, Germany, Berlin, 7 Pferd St
Celine, Poirot, 1987-05-20, France, Paris, 101 Longue St
Hans, Johan, 1997-03-15, Germany, Frankfurt, 12 Weiss St
Helen, Peters, 1987-07-27, UK, London, 9 Steward St
Isabel, Werth, 1975-04-06, Germany, Frankfurt, 87 Mein St
John, Smith, 1985-03-18, UK, Liverpool, 33 Upside St
John, Smith, 2001-01-14, Ireland, Dublin, 98 Violet St
Mark, McDonald, 1997-04-26, Ireland, Dublin, 99 Violet St
Mary, Peterson, 1972-04-21, France, Paris, Place Elysee
Peter, Andrews, 1994-04-24, Ireland, Dublin, 78 Oak St
Pierre, Michel, 1978-10-30, France, Lyon, 10 Rue de Mer St
Polly, Well, 1974-05-30, UK, Liverpool, 7 Upper St
Stuart, Black, 1981-08-03, UK, London, 10 Baker St
Walter, Brown, 1974-04-20, Ireland, Dublin, 4 St Patrick St
